import { Meteor } from 'meteor/meteor';
import React from 'react';
import ReactDOM , { render } from 'react-dom';
import { Router, Route } from 'react-router';
import createBrowserHistory from 'history/createBrowserHistory';

import App from '../imports/ui/App.js';
import MyRoutes from '../imports/client/routes.js';
import '../imports/client/files.collection.js';
import { _app, Collections }  from '../imports/lib/core.js';


Meteor.startup(() => {

  // Register service worker
  import('../imports/ui/register-sw').then(() => {}); 
  
  ReactDOM.render(<App component={MyRoutes} />, document.getElementById('main'));  
});