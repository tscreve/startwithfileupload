import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

import { _app, Collections } from '../../imports/lib/core.js';

import MyCat from './categorie.js'
import MyRegion from './region.js'

export const Annonces = new Mongo.Collection('annonces');

SimpleSchema.setDefaultMessages({
  initialLanguage: 'fr',
  messages: {
    fr: {
      required: '{{label}} est requis',
      minString: '{{label}} doit comporter au moins {{min}} caracteres',
      maxString: '{{label}} ne doit pas depasser {{max}} caracteres',
      minNumber: '{{label}} doit etre au moins de {{min}}',
      maxNumber: '{{label}} ne peut depasser {{max}}',
      minNumberExclusive: '{{label}} must be greater than {{min}}',
      maxNumberExclusive: '{{label}} must be less than {{max}}',
      minDate: '{{label}} must be on or after {{min}}',
      maxDate: '{{label}} cannot be after {{max}}',
      badDate: '{{label}} is not a valid date',
      minCount: 'You must specify at least {{minCount}} values',
      maxCount: 'You cannot specify more than {{maxCount}} values',
      noDecimal: '{{label}} doit etre entier',
      noNumber: '{{label}} doit etre un nombre',
      notAllowed: '{{value}} is not an allowed value',
      expectedType: '{{label}} must be of type {{dataType}}',
      badAddress:"Entrez une adresse valide",
      noPrice:"Entrez un prix valide",
      diffPassword:"Les mots de passe ne correspondent pas",
      regEx({ label, regExp }) {
          switch (regExp) {
              case (SimpleSchema.RegEx.Email.toString()):
              case (SimpleSchema.RegEx.EmailWithTLD.toString()):
                  return "Cette adresse e-mail est incorrecte";
              case (SimpleSchema.RegEx.Domain.toString()):
              case (SimpleSchema.RegEx.WeakDomain.toString()):
                  return "Ce champ doit être un domaine valide";
              case (SimpleSchema.RegEx.IP.toString()):
                  return "Cette adresse IP est invalide";
              case (SimpleSchema.RegEx.IPv4.toString()):
                  return "Cette adresse IPv4 est invalide";
              case (SimpleSchema.RegEx.IPv6.toString()):
                  return "Cette adresse IPv6 est invalide";
              case (SimpleSchema.RegEx.Url.toString()):
                  return "Cette URL is invalide";
              case (SimpleSchema.RegEx.Id.toString()):
                  return "Cet identifiant alphanumérique est invalide";
              case (SimpleSchema.RegEx.ZipCode.toString()):
                  return "Ce code postal est invalide";
              case (SimpleSchema.RegEx.Phone.toString()):
                  return "Ce numéro de téléphone est invalide";
              default:
                  return "Ce champ a échoué la validation par Regex";
          }
      },
      keyNotInSchema: '{{name}} is not allowed by the schema',
    },
  }
});


let i,cat=[],o;
const obj=JSON.parse(MyCat);
const objRegion=JSON.parse(MyRegion);
// console.log(obj);

  for(o in obj){
    // console.log(obj[o]['id']);
    cat.push(obj[o]['id']);
    // console.log(obj[o]['text']);
    for(i=0;i<=obj[o]['sub'].length-1;i++){
        cat.push(obj[o]['sub'][i]['id']);
        // console.log(obj[o]['sub'][i]['text']);
    }
  }
// console.log(cat) 
Annonces.insertSchema = new SimpleSchema({
    date: {
        label:'Date :',
        type: Date,
        optional:true,
    },
    createBy:{
        type: String,
        optional:true,
    },
    type: {
        type: String,
        allowedValues: ['Offre', 'Demande'],
        defaultValue: 'Offre',
        label:"Type"
    },
    categorie: {
        type: String,
        allowedValues: cat,
        // defaultValue: cat[2],
        optional:true,
        label:"Choisissez une categorie"
    },
    title: {
        type: String,
        max: 100,    
        optional:true,   
        label:"Titre" 
    },
    description: {
        type: String,
        max: 1000,
        optional:true,
        label:"Description"
    },
    price:{
        type:String,
        // custom() {
        //   if (isNaN(this.value.replace(",","."))) {
        //     return "noPrice";
        //   }
        // }, 
        optional:true,
        label:"Prix"
    },
    address: {
        optional:true,
        label:'Adresse',
        type: String,
        max: 200,
        custom() {
          if (!this.value) {
            return "badAddress";
          }
        },
    },
    postcode: {
        // optional:true,
        label:'Code postal',
        type: String,
        max: 5,
        regEx:SimpleSchema.RegEx.ZipCode,
    },
    dept:{
        type:Array,
        optional: true,
    },
    city:{
        type:String,
        optional: true,
    },
    'dept.$':String,
    region:{
        type:Array,
        optional: true,
    },
    'region.$':String,
    latlng:{
        type: Object,
    },
    'latlng.lat': {
        type:Number,
        optional:true,
        custom() {
          if (!this.value || !this.field('latlng.lng').value) {
            return "badAddress";
          }
        },   
    },
    'latlng.lng': {
        type:Number,
        optional:true, 
    },
    photos: {
        type: Array,
        optional: true,
    },
    'photos.$': String,

    // photos: {
    //     type: Object,
    //     optional: true,
    // },
    // 'photos.$': Array,
    // 'photos.$.id': String,
});


// Annonces.searchSchema = new SimpleSchema({
//     categorie: {
//         type: String,
//         allowedValues: catnSubCat,
//     },
//     searchText: {
//       label:'Que cherchez vous ?',
//       type: String,
//       min:5,
//       max: 50,
//       optional: true,
//     },
//     region: {
//         type: String,
//         optional: true,
//     },
// });


// Annonces.insertSchema.messageBox.setLanguage('fr')
Annonces.attachSchema(Annonces.insertSchema, {selector: {schem: 'insert'}});


if (Meteor.isServer) {
  Annonces.rawCollection().createIndex( { title: "text", description: "text" });

  Meteor.publish('annonces.all', function annPublication() {
   if (Roles.userIsInRole(this.userId,['admin'])) {
      return Annonces.find();
    }      
  });  

  Meteor.publish('annonces.myAnnonces', function() {
    const userId = Meteor.userId();
    // console.log(Meteor.userId())
    if ( userId ) {
      return Annonces.find({
        createBy: userId
      },{});
    }
  });

  Meteor.methods({
   'oneAnnonce.get'(id) {
      let response={}

      let i,cat=[],mainCat="",subCat="",o;
      let obj=JSON.parse(MyCat);
      // console.log(obj);

      if(Meteor.isServer){
        response.props=Annonces.find({_id:id}).fetch();      
        let photos=response.props[0].photos
        let link=[];
        // console.log(response.props[0].categorie);

        response.cat=getCategorie(response.props[0].categorie)

        ///////////////////////////////////////////////////
        photos.map((p) => (
            link.push([Collections.files.findOne({_id:p}).link('preview2'),
                        Collections.files.findOne({_id:p}).link('thumbnail100'),
                        Collections.files.findOne({_id:p}).link('big'),
                        // Collections.files.findOne({_id:p}).fetch(),
                        Collections.files.findOne({_id:p}).get('meta')
                        ]) 
          ))  
        // console.log(link)  
        response.link=link;  
        return response
      }
    },

    'annonces.get'({curPage,nbRows,queryData}) {
      const region=queryData.region === 'All' || queryData.returnPlace === 'All' ? null : queryData.region
      const dept=queryData.returnPlace && queryData.returnPlace.substr(0,4) === 'dept' ? queryData.returnPlace.substr(5,2) : null
      // console.log(dept);
      console.log(queryData);

      let response={},query={};
      let cat=queryData.categorie;
      // Annonces.searchSchema.validate(queryData); 
      // console.log(cat)console
      let acat=[],withSub=false

      /////////////   CONSTRUCTION DE LA REQUETE
      ////////////   TEXTE A CHERCHER ///////  //
      if(queryData.searchText){
        query={ $or: [ { title: {$regex:queryData.searchText, $options: '-i'}},  { description: {$regex:queryData.searchText, $options: '-i'}} ] }
      }
      /////////////   CATEGORIE  /////////
      if(cat === "All"){
         response.cat=getCategorie(cat)
      }else {
        acat=cat.split(",")
        if(acat.length > 1){
          query.categorie={'$in':acat};
        }else{
          query.categorie=cat;
          withSub=true
        }
        response.cat=getCategorie(acat[0])
      }
      ////////////   TYPE  /////////
      query.type=queryData.type
      ////////////   REGION  /////////
      if(region){
        query.region= {'$in':[region]}
      }
      ////////////   DEPARTEMENT  /////////
      if(dept){
        query.dept= {'$in':[dept]}
      }
      console.log(query)
      /////////////  FIN CONSTRUCTION DE LA REQUETE

      response.cat.withSub=withSub
      response.count=Annonces.find(query).count();
      response.annonces=Annonces.find(
        query,
        {sort:{'date': -1},skip:curPage*nbRows,limit:nbRows }).fetch();
      return response
    },

    'favorites.get'(){    
      const fav=Meteor.user().favorites ? Meteor.user().favorites : [];
      let response={},i=0,newFav=[];

      response=Annonces.find(
        {_id:{'$in':fav}},
        {sort:{'date': -1}}).fetch();
      /////////   SUPPRESSION DES FAVORIS DONT 
      /////////   LES ANNONCES ONT ETE SUPPRIMES
      if(response.length !== fav.length){
        for(i=0;i<response.length;i++){
          // console.log(response[i]._id)
          newFav.push(response[i]._id)
        } 
        Meteor.users.update(Meteor.userId(),{
          $set:{
            favorites:newFav
          }
        }); 
      }
      /////////////////////////////////////////////
      return response
    },

    'annonces.insert'(data) { 
      const dept= data.postcode.substr(0,2) === '97' ? data.postcode.substr(0,3) : data.postcode.substr(0,2);
      const location=getRegion(dept);
      // console.log(location);
      // Make sure the user is logged in before inserting 
      if (! this.userId) {
        throw new Meteor.Error('not-authorized');
      }
      data.createBy=Meteor.userId()
      data.date=new Date();
      data.dept=location[1];
      data.region=location[0]
   
      return Annonces.insert(data, { selector: {schem: 'insert'},validationContext: "insertForm" }, (error, result) => {  
      if(error){
        // console.log(error);
        return error;
        // console.log(Annonces.simpleSchema().namedContext("insertForm").validationErrors());  
        //The list of errors is available by calling Books.simpleSchema().namedContext("insertForm").validationErrors()
      }
      // console.log(result);
      return result;
      });
    },

    'annonces.update'(id,data){
      const dept= data.postcode.substr(0,2) === '97' ? data.postcode.substr(0,3) : data.postcode.substr(0,2);
      const location=getRegion(dept);
      const annonce = Annonces.findOne(id);
      if (annonce.createBy !== this.userId) {
        // If the task is private, make sure only the owner can delete it
        throw new Meteor.Error('not-authorized');
      }

      // console.log(id,data)
      if (! this.userId) {
        throw new Meteor.Error('not-authorized');
      }

      Annonces.update(id, { $set: { 
                                      title: data.title,
                                      categorie: data.categorie,
                                      postcode: data.postcode,
                                      city: data.city,
                                      photos: data.photos,
                                      type: data.type,
                                      description: data.description,
                                      price: data.price,
                                      address: data.address,
                                      latlng: data.latlng,
                                      dept:location[1],
                                      region:location[0],
                                    }},
                            { selector: {schem: 'insert'},validationContext: "updateForm" }, (error, result) => {  
      if(error){
        console.log(error);
        return error;
        console.log(Annonces.simpleSchema().namedContext("updateForm").validationErrors());  
        //The list of errors is available by calling Books.simpleSchema().namedContext("insertForm").validationErrors()
      }
      // console.log(result);
      return result;
      });
    },

    'annonces.remove'(id) {
      const annonce = Annonces.findOne(id);
      if (annonce.createBy !== this.userId) {
        throw new Meteor.Error('not-authorized');
      }
      if(annonce.photos.length>0){
        console.log(annonce.photos)
        for(i=0;i<annonce.photos.length;i++){
          Collections.files.remove(annonce.photos[i]);        
        }
      }
      Annonces.remove(id);
    },
  })





/////////////////////////   FUNCTIONS   //////////////////////////
  function getCategorie(queryCat){
    let categorie={},subCat=[],mainCat="",cat=[]
      /////////// recherche de la categorie principale a partir de l'id la de categorie

        for(o in obj){
          subCat=[]
          //////     TOUTES CATEGORIES, id='All'    //////
          // if(queryCat==='All'){
          //   mainCat=[obj[o]['id'],obj[o]['text']]
          //   cat=[]
          //   subCat=[]
          // }else{}   
          /////////////////////////////////////////       
            for(i=0;i<=obj[o]['sub'].length-1;i++){
               subCat.push(obj[o]['sub'][i]['id'])
               if(queryCat === obj[o]['sub'][i]['id']){
                  mainCat=[obj[o]['id'],obj[o]['text']]
                  cat= [obj[o]['sub'][i]['id'], obj[o]['sub'][i]['text']]
                }
            }
          
          if(mainCat !== ''){ break; }
        } 
        categorie.mainCat=mainCat;
        categorie.subCat=subCat;
        categorie.currentCat=cat;   
      return categorie
  }

  function getRegion(queryRegion){
    let region="",dept="";
    let deptId=queryRegion
    /////////// recherche de la region a partir de l'id du departement

    if(queryRegion === '20'){
     deptId="2A"
    }

    for(o in objRegion){
      region = ""
        for(i=0;i<=objRegion[o]['dept'].length-1;i++){              
           if(deptId === objRegion[o]['dept'][i]['id']){                  
              region=[objRegion[o]['id'],objRegion[o]['text']]       
              dept=[objRegion[o]['dept'][i]['id'],objRegion[o]['dept'][i]['text']]        
            }                
        }  
        if(region !== ""){ break; } 
    }
    return [region,dept]
  }
///////////////////////// END FUNCTIONS   //////////////////////////
}