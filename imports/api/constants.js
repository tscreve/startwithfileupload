/**
 * @namespace Constants
 * @summary constants used across the app.
 */
const Constants = {
  SITE_BRAND: 'tomdev',
  DOMAIN_NAME: 'www.tomdev.fr',
  SUPPORT_EMAIL: 'support@tomdev.fr',
  SITE_TWITTER: '@tomdev',
  AUTH_SERVICES: ['password', 'facebook'],
  ALL_ROLES: ['admin', 'normal'],
};

export default Constants;
