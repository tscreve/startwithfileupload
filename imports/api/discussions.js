import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
 

import { Annonces } from './annonces.js';
import { _app, Collections } from '/imports/lib/core.js';

export const Discussions = new Mongo.Collection('discussions');

if (Meteor.isServer) {
  Meteor.publish('discussions.one', function (discussionId) {
    // console.log(id)
      return Discussions.find({
        _id:discussionId,
      });
  });

  Meteor.publish('discussions.all', function () {
      return Discussions.find({
        '$or':[
            {createBy:this.userId},{objectOwner:this.userId}
        ]
      });
  });

  Meteor.methods({
    'discussions.insert'(text,objectId) {   
      // Make sure the user is logged in before inserting a task
      if (!this.userId) {
        throw new Meteor.Error('not-authorized');
      }

    if(objectId){
        const objectOwner=Annonces.findOne({_id:objectId},{fields:{createBy:1}}).createBy
        if(objectOwner=== this.userId){
          throw new Meteor.Error('not-authorized, current user is object owner');
        }
        // const createBy=this.userId

        return Discussions.insert({   
            lastModified:new Date(),       
            createBy:this.userId,
            object:objectId,
            objectOwner:objectOwner,
            messages:[{author:this.userId,date: new Date(),text:text}],
          },
          (err,res) => {
            if(err){
              return err;
            }
            return res
          }
        ); 
      }
    },

    'discussions.update'(text,discussionId) {   
      // Make sure the user is logged in before inserting a task
      if (!this.userId) {
        throw new Meteor.Error('not-authorized');
      }

      if(discussionId){
        // const discussion = Discussions.findOne({object:id,createBy:this.userId});
        const discussion = Discussions.findOne({_id:discussionId});

        if(discussion){
          // console.log(discussion.messages)
          discussion.messages.push({author:this.userId,date: new Date(),text:text})
          Discussions.update(discussion._id,{$set:{
                                                    messages:discussion.messages,
                                                    lastModified:new Date(),
                                                  }},);
        }
      }
    },

    'discussions.getFromObject'(idObject) {   
      // Make sure the user is logged in before inserting a task
      if (!this.userId) {
        throw new Meteor.Error('not-authorized');
      }
        const discussion = Discussions.findOne({object:idObject,createBy:this.userId},{fields:{_id:1}})._id;
        return discussion
    },

    'discussions.getPreview'(id) {   
      const res=Annonces.findOne({_id:id},{fields:{photos:1}})
      // console.log(res)
      if(res){
        let link;
        if((Collections.files.find({_id:res.photos[0]}).count()) > 0 ){
          link=Collections.files.findOne({_id:res.photos[0]}).link('thumbnail100');
        }else{
          link=null;
        }
        return link
      }
    },

    'discussions.remove'(id) {   	
     	const discussion = Discussions.findOne(id);
        if (discussion.createBy !== this.userId) {
          // If the task is private, make sure only the owner can delete it
          throw new Meteor.Error('not-authorized');
        }

        Discussions.remove(id);
      },
  });
}