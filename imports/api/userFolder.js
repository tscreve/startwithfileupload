import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
 
export const Folders = new Mongo.Collection('userFolders');


if (Meteor.isServer) {
  // This code only runs on the server
  // Only publish tasks that are public or belong to the current user
  Meteor.publish('userFolders', function foldersPublication() {
    return Folders.find({userId:this.userId});
  });
}

Meteor.methods({
  'userFolders.insert'(path,name) {
    check(name, String);
    let b= Folders.find({path:path,name:name}).fetch();
    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }
    console.log(path,name);
   console.log(b.length);
   if(b<1){
       
    Folders.insert({
      path,
      name,   
      userId: this.userId,
    });
    
  }else{
    return false;
    
  }

  },
  'userFolders.remove'(Id) {
    check(Id, String);
 	
 	// const folder = Folders.findOneId);
  //   if (task.private && task.owner !== this.userId) {
  //     // If the task is private, make sure only the owner can delete it
  //     throw new Meteor.Error('not-authorized');
  //   }

    Folders.remove(Id);
  },

});