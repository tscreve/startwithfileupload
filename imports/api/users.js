import { Meteor }            from 'meteor/meteor'
import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

import { _app, Collections } from '../../imports/lib/core.js';

const userProfileSchema = new SimpleSchema({
    name: {
        type: String,
        optional: true
    },
    birthday: {
        type: Date,
        optional: true
    },
    gender: {
        type: String,
        allowedValues: ['Male', 'Female'],
        optional: true
    },
    organization : {
        type: String,
        optional: true
    },
    website: {
        type: String,
        regEx: SimpleSchema.RegEx.Url,
        optional: true
    },
    bio: {
        type: String,
        optional: true
    },
});

export const userSchema = new SimpleSchema({
	_id: { 
		type: String 
	},
    username: {
        type: String,
        // For accounts-password, either emails or username is required, but not both. It is OK to make this
        // optional here because the accounts-password package does its own validation.
        // Third-party login packages may not require either. Adjust this schema as necessary for your usage.
        optional: true
    },
    emails: {
        type: Array,
        // For accounts-password, either emails or username is required, but not both. It is OK to make this
        // optional here because the accounts-password package does its own validation.
        // Third-party login packages may not require either. Adjust this schema as necessary for your usage.
        optional: true
    },
    "emails.$": {
        type: Object
    },
    "emails.$.address": {
        type: String,
        regEx: SimpleSchema.RegEx.Email
    },
    "emails.$.verified": {
        type: Boolean
    },
    createdAt: {
        type: Date
    },
    profile: {
        type: userProfileSchema,
        optional: true
    },
    // Make sure this services field is in your schema if you're using any of the accounts packages
    services: {
        type: Object,
        optional: true,
        blackbox: true
    },
    // Add `roles` to your schema if you use the meteor-roles package.
    // Option 1: Object type
    // If you specify that type as Object, you must also specify the
    // `Roles.GLOBAL_GROUP` group whenever you add a user to a role.
    // Example:
    // Roles.addUsersToRoles(userId, ["admin"], Roles.GLOBAL_GROUP);
    // You can't mix and match adding with and without a group since
    // you will fail validation in some cases.
    roles: {
        type: Object,
        optional: true,
        blackbox: true
    },
    // In order to avoid an 'Exception in setInterval callback' from Meteor
    heartbeat: {
        type: Date,
        optional: true
    }
});



if (Meteor.isServer) {

Meteor.users.attachSchema(userSchema);

	Accounts.onCreateUser((options, user) => {
	  console.log('\nsign up attempt:', new Date());
	  // Handle password signup
	  if (user.services.password) {
	    // const email = user.emails[0].address;
	    // const name = email.split('@')[0];
	    // console.log(
	    //   '\nservice --> password',
	    //   '\nname:', name,
	    //   '\nemail:', email,
	    // );

	    // // Extend user's profile by adding default name and avatar
	    // const profile = {
	    //   name,
	    //   // avatar: 'http://pixeljoint.com/files/icons/magic_johnson.gif',
	    // };
	    return Object.assign({}, user);
	  }
	  // Handle facebook signup
	  if (user.services.facebook) {
	    const { id, name, gender, email } = user.services.facebook;
	    console.log(
	      '\nservice --> facebook',
	      '\nname:', name,
	      '\nid:', id,
	      '\ngender:', gender,
	      '\nemail:', email,
	    );
	    // Extend user's profile by adding facebook data
	    const profile = {
	      name,
	      gender,
	      avatar: `http://graph.facebook.com/${id}/picture/`,
	    };
	    return Object.assign({}, user, { profile });
	  }
	  // Throw in case of a different service
	  throw new Error(401, 'Sign up attempt with service different than facebook or password');
	});

	Accounts.urls.resetPassword = function reset(token) {
	  return Meteor.absoluteUrl('reset-password/' + token);
	};

	Accounts.urls.verifyEmail = function reset(token) {
	  return Meteor.absoluteUrl('verify-email/' + token);
	};

	Accounts.validateLoginAttempt((attempt) => {
	   if(attempt.user){
	    if (attempt.user.emails[0].verified === true) {
	          return true;
	      } else {
	        Accounts.sendVerificationEmail( attempt.user._id );
	          throw new Meteor.Error('email-not-verified', 'You must verify your email address before you can log in');
	      }
	   }else{
	    // return false;
	   }
	   return false;
	});

	Accounts.validateNewUser((user) => {
	  userSchema.validate(user);

	  // Return true to allow user creation to proceed
	  return true;
	});

////////////////////////////////////////////////////////////////////////////////////

	Meteor.publish('users.all', function() {
	   if (Roles.userIsInRole(this.userId,['admin'])) {
	      return Meteor.users.find();
	    }   
	});

	Meteor.publish('users.favorites', function() {
	  const userId = Meteor.userId();
	  // console.log(Meteor.userId())
	  if ( userId ) {
	    return Meteor.users.find({
	      _id: userId
	    }, {
	      fields: {favorites:1}
	    });
	  }
	});

////////////////////////////////////////////////////////////////////////////////////

	Meteor.methods({
	  'favorite'(id){
	    let fav=Meteor.user().favorites ? Meteor.user().favorites : []
	    
	    if(id !== -1){
	        if(fav.indexOf(id) !== -1){  
	          // console.log(fav.indexOf(id))    
	          fav.splice(fav.indexOf(id),1)
	        }else{
	          fav.push(id)
	        }
	        
	        Meteor.users.update(Meteor.userId(),{
	          $set:{
	            favorites:fav
	          }
	        });
	    }

	    // console.log(Meteor.user().favorite)
	    return Meteor.user().favorites
	  },
	})
}




// Meteor.methods({
//   'sendVerificationLink'(error ,response) {
//     let userId = Meteor.userId();
//     console.log(userId);
//     if ( userId ) {
//      return Accounts.sendVerificationEmail( userId );
//     }
//   },  
// });