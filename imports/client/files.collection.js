import { _ }                 from 'meteor/underscore';
import { Meteor }          from 'meteor/meteor';
import { Random }            from 'meteor/random';
import { filesize }          from 'meteor/mrt:filesize';
import { FilesCollection } from 'meteor/ostrio:files';

import { _app, Collections } from '../lib/core.js';

Collections.files = new FilesCollection({
  // debug: true,
  collectionName: 'UserFiles',
  allowClientCode: true, // Disallow remove files from Client
  onBeforeUpload: function (file) {
    // Allow upload files under 10MB, and only in png/jpg/jpeg formats
    // if (file.size <= 1024 * 1024 * 10 && /png|jpe?g/i.test(file.extension)) {
    //   return true;
    // }
    // return 'Please upload image, with size equal or less than 10MB';


      return true;
  }
});


  