import React from 'react';
import ReactDOM from 'react-dom';
import { Route,Redirect,Switch } from 'react-router';
import Home from '../ui/vues/Home1.js';
import Messages from '../ui/vues/vueMessages.js';
import Discussions from '../ui/vues/vueDiscussions.js';
import VueAnnonce from '../ui/vues/vueAnnonce.js';
import ListeAnnonces from '../ui/vues/vueListeAnnonces.js';
import Favoris from '../ui/vues/vueFavoris.js';
import MesAnnonces from '../ui/vues/vueMesAnnonces.js';
import DeposerAnnonce from '../ui/vues/deposerAnnonce.js';
import Admin from '../ui/vues/vueAdmin.js';
import AppBar from '../ui/mytoolbar.js';
import Login from '../ui/vues/login.js';
import VerifyEmail from '../ui/vues/verify-email.js';

// import Files from '../ui/upload/UploadList.js';
// import UploadForm from '../ui/upload/UploadForm.js';

const isLoggedIn = () => {
  return Meteor.userId() !== null; 
};

 const MyRoutes = props => (
    <div className="container">
      <Switch> 
         <Route path="/login" render={() => (isLoggedIn() ? <Redirect to="/Home"/> : <Login/>)}  />            
         <Route path="/signup" render={() => (isLoggedIn() ? <Redirect to="/Home"/> : <Login/>)}  />            
         <Route path="/verify-email/:token" component={VerifyEmail}/>
         <Route path="/reset-password/:token" render={() => (<Login myview='reset'/>)} />  
         <Route exact path="/" render={() => (<div><AppBar itemSelected={false}/><Home/></div>)}  />  
         <Route path="/Home" render={() => (<div><AppBar itemSelected={false}/><Home/></div>)}  /> 
         <Route path="/deposer_une_annonce" render={() => (<div><AppBar itemSelected={0}/><DeposerAnnonce context="new"/></div>)}  />  
         <Route path="/modifier_une_annonce/:id" render={() => (<div><AppBar/><DeposerAnnonce context="edit"/></div>)}  />  
         
 
         <Route path="/favorite" render={() => (isLoggedIn() ? <div><AppBar itemSelected={3}/><Favoris/></div> : <Login/>)}  /> 
         <Route path="/mes_annonces" render={() => (isLoggedIn() ? <div><AppBar/><MesAnnonces/></div> : <Login/>)}  /> 
         <Route path="/admin" render={() => (isLoggedIn() ? <div><AppBar/><Admin/></div> : <Login/>)}  /> 



         <Route path="/annonce/:id" render={() => (<div><AppBar/><VueAnnonce/></div>)}  />  
         <Route path="/discussions" render={() => (<div><AppBar itemSelected={4}/><Discussions/></div>)}  />  
         <Route path="/messages" render={() => (<div><AppBar/><Messages/></div>)}  />  
 
 
 
  
 
 
         
         <Route path="/offre" render={() => (<div><AppBar itemSelected={1}/><ListeAnnonces type="Offre"/></div>)}  />  
         <Route path="/demande" render={() => (<div><AppBar itemSelected={2}/><ListeAnnonces type="Demande"/></div>)}  />  
         <Route path="/offres/region/:region" render={() => (<div><AppBar itemSelected={1}/><ListeAnnonces type="Offre"/></div>)} />      
         <Route path="/offres/categorie/:categorie" render={() => (<div><AppBar itemSelected={1}/><ListeAnnonces type="Offre"/></div>)} />       
         <Route path="/demandes/categorie/:categorie" render={() => (<div><AppBar itemSelected={2}/><ListeAnnonces type="Demande"/></div>)} />       
      </Switch>           
    </div>
);

export default MyRoutes;