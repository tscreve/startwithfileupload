import { _ }      from 'meteor/underscore';
import { _app }   from '/imports/lib/core.js';
import { check }  from 'meteor/check';
import { Meteor } from 'meteor/meteor';

import fs from 'fs-extra';
import gm from 'gm';
//Some platforms may bundle ImageMagick into their tools (like Heroku). In this case you may use GraphicsMagick as Imagemagick in this way:
//npm install gm --save and then where you use it:
//const gm = require('gm');
//const im = gm.subClass({ imageMagick: true });
//Please note that GM was considered slightly faster than IM so before you chose convenience over performance read the latest news about it.
//https://mazira.com/blog/comparing-speed-imagemagick-graphicsmagick

const bound = Meteor.bindEnvironment((callback) => {
  return callback();
});

_app.createThumbnails = (collection, fileRef, cb) => {
  check(fileRef, Object);

  let isLast = false;
  const finish = (error) => {
    bound(() => {
      if (error) {
        console.error('[_app.createThumbnails] [finish]', error);
        cb && cb (error);
      } else {
        if (isLast) {
          cb && cb(void 0, fileRef);
        }
      }
      return true;
    });
  };

  fs.exists(fileRef.path, (exists) => {
    bound(() => {
      if (!exists) {
        throw new Meteor.Error('File ' + fileRef.path + ' not found in [createThumbnails] Method');
      }
      let orient="";
      const image = gm(fileRef.path);
      const sizes = {
        thumbnail100: {
          width: 100,
          square: true
        },
        preview: {
          width: 300,
          square: true
        },
        preview2: {
          width: 500,
          square: true
        },
        big: {
          width: 900,
          // square: true
        }
      };

      image.orientation((err,value) => {
        // console.log(value);
        orient=value;
      });

      image.size((error, features) => {
        console.log(features)
        bound(() => {
          if (error) {
            console.error('[_app.createThumbnails] [_.each sizes]', error);
            finish(new Meteor.Error('[_app.createThumbnails] [_.each sizes]', error));
            return;
          }

          let i = 0;
          collection.collection.update(fileRef._id, {
            $set: {
              'meta.width': features.width,
              'meta.height': features.height,
              'versions.original.meta.width': features.width,
              'versions.original.meta.height': features.height
            }
          }, _app.NOOP);

          _.each(sizes, (size, name) => {
            const path = (collection.storagePath(fileRef)) + '/' + name + '-' + fileRef._id + '.' + fileRef.extension;
            const copyPaste = () => {
              fs.copy(fileRef.path, path, (fsCopyError) => {
                bound(() => {
                  if (fsCopyError) {
                    console.error('[_app.createThumbnails] [_.each sizes] [fs.copy]', fsCopyError);
                    finish(fsCopyError);
                    return;
                  }

                  const upd = { $set: {} };
                  upd['$set']['versions.' + name] = {
                    path: path,
                    size: fileRef.size,
                    type: fileRef.type,
                    extension: fileRef.extension,
                    meta: {
                      width: features.width,
                      height: features.height
                    }
                  };

                  collection.collection.update(fileRef._id, upd, (colUpdError) => {
                    ++i;
                    if (i === Object.keys(sizes).length) {
                      isLast = true;
                    }
                    finish(colUpdError);
                  });
                });
              });
            };

            if (/png|jpe?g/i.test(fileRef.extension)) {
              const img = gm(fileRef.path)
                .quality(70)
                .define('filter:support=2')
                .define('jpeg:fancy-upsampling=false')
                .define('jpeg:fancy-upsampling=off')
                .define('png:compression-filter=5')
                .define('png:compression-level=9')
                .define('png:compression-strategy=1')
                .define('png:exclude-chunk=all')
                .autoOrient()
                .noProfile()
                .strip()
                .dither(false)
                .interlace('Line')
                .filter('Triangle');

              const updateAndSave = (upNSaveError) => {
                bound(() => {
                  if (upNSaveError) {
                    console.error('[_app.createThumbnails] [_.each sizes] [img.resize]', upNSaveError);
                    finish(upNSaveError);
                    return;
                  }
                  fs.stat(path, (fsStatError, stat) => {
                    bound(() => {
                      if (fsStatError) {
                        console.error('[_app.createThumbnails] [_.each sizes] [img.resize] [fs.stat]', fsStatError);
                        finish(fsStatError);
                        return;
                      }

                      gm(path).size((gmSizeError, imgInfo) => {
                        bound(() => {
                          if (gmSizeError) {
                            console.error('[_app.createThumbnails] [_.each sizes] [img.resize] [fs.stat] [gm(path).size]', gmSizeError);
                            finish(gmSizeError);
                            return;
                          }
                          const upd = { $set: {} };
                          upd['$set']['versions.' + name] = {
                            path: path,
                            size: stat.size,
                            type: fileRef.type,
                            extension: fileRef.extension,
                            name: fileRef.name,
                            meta: {
                              width: imgInfo.width,
                              height: imgInfo.height
                            }
                          };

                          collection.collection.update(fileRef._id, upd, (colUpdError) => {
                            ++i;
                            if (i === Object.keys(sizes).length) {
                              isLast = true;
                            }
                            finish(colUpdError);
                          });
                        });
                      });
                    });
                  });
                });
              };

              if (!size.square) {
                if (features.width > size.width) {
                  if(orient === 'TopLeft'){
                    img.resize(size.width).interlace('Line').write(path, updateAndSave);
                  }else{
                    img.resize(null,size.width).interlace('Line').write(path, updateAndSave);
                  }                  
                } else {
                  copyPaste();
                }
              } else {
                let x = 0;
                let y = 0;
                const w= (orient === "RightTop") ? features.height : features.width;
                const h= (orient === "RightTop") ? features.width : features.height;
                // console.log(features.width,w)
                // console.log(features.height,h)
                const widthRatio  = w / size.width;
                const heightRatio = h / size.width;
                let widthNew      = size.width;
                let heightNew     = size.width;

                if (heightRatio < widthRatio) {
                  console.log('width')
                  widthNew = (size.width * w) / h;
                  x = (widthNew - size.width) / 2;
                }

                if (heightRatio > widthRatio) {
                  console.log('height')
                  heightNew = (size.width * h) / w;
                  y = (heightNew - size.width) / 2;
                }
                // console.log(orient)
                img
                  .resize(widthNew, heightNew)
                  .crop(size.width, size.width, x, y)
                  .interlace('Line')
                  .write(path, updateAndSave);
              }
            } else {
              copyPaste();
            }
          });
        });
      });
    });
  });
  return true;
};
