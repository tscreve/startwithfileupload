import React from 'react';
import { Router } from 'react-router-dom';
import createBrowserHistory from 'history/createBrowserHistory';

import {MuiThemeProvider,createMuiTheme} from '@material-ui/core/styles';
import blue from '@material-ui/core/colors/blue';
import pink from '@material-ui/core/colors/pink';
import red from '@material-ui/core/colors/red';
// import CssBaseline from '@material-ui/core/CssBaseline';

// Given that we are implementing App Shell Architecture and, therefore,
// injecting (via reactDOM.render) the Header, Menu and Main components into
// different HTML elements, we need a way to share the router 'history' among
// all three mentioned components.
// As a default, for every invocation of 'BrowserRouter', there will be new
// 'history' instance created. Then, changes in the 'history' object in one
// component won't be available in the other components. To prevent this, we are
// relying on 'Router' component instead of 'BrowserRouter' and defining our
// custom 'history' object by means of 'createBrowserHistory' function. Said
// 'history' object is then passed to every invocation of 'Router' and therefore
// the same 'history' object will be shared among all three mentioned components.
const theme = createMuiTheme({
  palette: {
    primary:  blue,
    secondary: pink,
    // type: 'dark',
  },
  typography: {
    useNextVariants: true,
  },
});
const history = createBrowserHistory();

const App = ({ component }) => (
  <MuiThemeProvider theme={theme}>  

  <Router history={history}>  	
  		{React.createElement(component, { })}   
  
  </Router>
</MuiThemeProvider>
);

export default App;
