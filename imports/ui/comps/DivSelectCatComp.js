import React, { Component } from 'react';
import { withRouter,Link,Redirect } from 'react-router-dom';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import MyCat from '../../api/categorie.js';

import Grid from '@material-ui/core/Grid';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import InboxIcon from '@material-ui/icons/Inbox';
import DraftsIcon from '@material-ui/icons/Drafts';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  mainCat:{
    backgroundColor:'lightblue',
    marginTop:5,
    fontSize: 15,
    fontWeight: 'bold',
  },
  homeContext:{
    backgroundColor:'white',
    marginTop:5,
  },
  searchContext:{
    marginTop:-25,
    zIndex:100,
    backgroundColor:'white',
    borderRadius:5,
    border:'1px solid lightblue',
  }
});

class DivSelectCat extends Component {
  constructor(props) {
    super(props);

    this.setWrapperRef = this.setWrapperRef.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);

    // console.log(this.props.context)

    let i,cat1=[],cat2=[],cat3=[],cat4=[],subCat=[],temp=[],o;
    let obj=JSON.parse(MyCat);

    for(o in obj){
      subCat=[]
      temp=[]
      // console.log(obj[o]);
      if(obj[o]['col'] === 1){
        for(i=0;i<=obj[o]['sub'].length-1;i++){          
            temp.push([obj[o]['sub'][i]['id'],obj[o]['sub'][i]['text'],obj[o]['id']]);
            subCat += (obj[o]['sub'][i]['id']) + "," ;
            // console.log(obj[o]['sub'][i]['text']);
        }
        // console.log(subCat)
        subCat = subCat.length > 0 ? subCat.substr(0,subCat.length-1) : ''  
        let v = (obj[o]===obj[0]) ? "All" : subCat
        cat1.push([v,obj[o]['text'].toUpperCase(),obj[o]['id'],'main']);
        cat1=cat1.concat(temp)
        // console.log(cat1)

      }else if(obj[o]['col'] === 2){       
        for(i=0;i<=obj[o]['sub'].length-1;i++){
            temp.push([obj[o]['sub'][i]['id'],obj[o]['sub'][i]['text'],obj[o]['id']]);
            subCat += (obj[o]['sub'][i]['id']) + "," ;
        }
        subCat = subCat.length > 0 ? subCat.substr(0,subCat.length-1) : ''  
        cat2.push([subCat,obj[o]['text'].toUpperCase(),obj[o]['id'],'main']);
        cat2=cat2.concat(temp)

      }else if(obj[o]['col'] === 3){
        for(i=0;i<=obj[o]['sub'].length-1;i++){
            temp.push([obj[o]['sub'][i]['id'],obj[o]['sub'][i]['text'],obj[o]['id']]);
            subCat += (obj[o]['sub'][i]['id']) + "," ;
        }
        subCat = subCat.length > 0 ? subCat.substr(0,subCat.length-1) : ''  
        cat3.push([subCat,obj[o]['text'].toUpperCase(),obj[o]['id'],'main']);
        cat3=cat3.concat(temp)

      }else if(obj[o]['col'] === 4){
        for(i=0;i<=obj[o]['sub'].length-1;i++){
            temp.push([obj[o]['sub'][i]['id'],obj[o]['sub'][i]['text'],obj[o]['id']]);
            subCat +=  (obj[o]['sub'][i]['id']) + "," ;
        }
        subCat = subCat.length > 0 ? subCat.substr(0,subCat.length-1) : ''  
        cat4.push([subCat,obj[o]['text'].toUpperCase(),obj[o]['id'],'main']);
        cat4=cat4.concat(temp)
      }
    } 

    this.state = { 
      listCat1:cat1,
      listCat2:cat2,
      listCat3:cat3,
      listCat4:cat4,
      // curCat:this.props.curCat,
    };
  }


  /**
   * Set the wrapper ref
   */
  setWrapperRef(node) {
    this.wrapperRef = node;
  }
  /**
   * Alert if clicked on outside of element
   */
  // handleClickOutside(event) {
  //   if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
  //     alert('You clicked outside of me!');
  //   }
  // }

  handleClickOutside(e){
    if (this.wrapperRef && !this.wrapperRef.contains(e.target)) {
      this.props.hideDiv(e);
    }
    
  }

  handleClick(e,c){
    let o={target:{'value':c}}
    // console.log(c);
    this.props.onChange(o);
    this.props.hideDiv(e);
  }

  componentDidUpdate(){

    // console.log(this.state.view);
  }

  componentDidMount(){
    document.addEventListener('mousedown', this.handleClickOutside);

  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
  }


  render() {
    const { classes,context } = this.props;
    const {listCat1,listCat2,listCat3,listCat4,view} = this.state;


    // console.log(listCat1,listCat2,listCat3,listCat4);
    // console.log(listCat1);




     return (
      <div ref={this.setWrapperRef} className={classes.container} >
      <Grid container spacing={8} className={ context==='search' ? classes.searchContext : classes.homeContext}>

        <Grid item xs={3} >
        <List >
          {
            listCat1.map((c,key )=> ( 

            <ListItem 
              className={c[3]==='main' ? classes.mainCat : null} 
              button                     
              key={key}
              value={c[2] + "," + c[0]}
              component={Link} 
              to={"/offres/categorie/" + [c[2],c[0]] + ""}
              onClick={(e) => this.handleClick(e,c[2] + "," + c[0])}
            >{c[1]}
            </ListItem>
          ))
          }
        </List>

        </Grid>
          
        <Grid item xs={3}>
        <List>
          {
            listCat2.map((c,key )=> (                        
            <ListItem 
              className={c[3]==='main' ? classes.mainCat : null} 
              button                     
              key={key}
               value={c[2] + "," + c[0]}
              component={Link} 
              to={"/offres/categorie/" + [c[2],c[0]] + ""}
              onClick={(e) => this.handleClick(e,c[2] + "," + c[0])}
            >{c[1]}
            </ListItem>
          ))
          }
        </List>
        </Grid>

        <Grid item xs={3}>
        <List>
          {
            listCat3.map((c,key )=> (                        
            <ListItem 
              className={c[3]==='main' ? classes.mainCat : null} 
              button                     
              key={key}
               value={c[2] + "," + c[0]}
              component={Link} 
              to={"/offres/categorie/" + [c[2],c[0]] + ""}
              onClick={(e) => this.handleClick(e,c[2] + "," + c[0])}
            >{c[1]}
            </ListItem>
          ))
          }
        </List>
        </Grid>

        <Grid item xs={3}>
        <List>
          {
            listCat4.map((c,key )=> (                        
            <ListItem 
              className={c[3]==='main' ? classes.mainCat : null} 
              button                     
              key={key}
               value={c[2] + "," + c[0]}
              component={Link} 
              to={"/offres/categorie/" + [c[2],c[0]] + ""}
              onClick={(e) => this.handleClick(e,c[2] + "," + c[0])}
            >{c[1]}
            </ListItem>
          ))
          }
        </List>
        </Grid>
      </Grid> 
      </div>
    );
  }
}

DivSelectCat.propTypes = {
   // children: PropTypes.element.isRequired,
  hideDiv: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(DivSelectCat);