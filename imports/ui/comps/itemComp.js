import React, { Component } from 'react';
import { withRouter,Link } from 'react-router-dom';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import compose from 'recompose/compose';

import { _app, Collections } from '../../lib/core.js';
import MyCat from '../../api/categorie.js';

import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import withWidth,{ isWidthUp } from '@material-ui/core/withWidth';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import IconButton from '@material-ui/core/IconButton';
import Checkbox from '@material-ui/core/Checkbox';
import FavoriteBorder from '@material-ui/icons/FavoriteBorder';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';


import Favorite from '@material-ui/icons/Favorite';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import MessageIcon from '@material-ui/icons/Forum';

const styles = theme => ({
  root: {
    padding:0,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  paper: {
    marginTop:10,
  },
  thumbnail:{
    maxWidth:150,
    minWidth:150,
    backgroundColor:'gray',  
    [theme.breakpoints.down('xs')]: {
      maxWidth:100,
      minWidth:100,
    },  
  },
  progressContainer:{
    width:150,
    height:150,
    [theme.breakpoints.down('xs')]: {
      width:100,
      height:100,
    }, 
  },
  progress:{
    position:'relative',
    top:53,
    left:53,
    [theme.breakpoints.down('xs')]: {
      top:28,
      left:28,
    }, 
  },
  img: {
    width: '100%',
  },
  button:{
    textTransform:'none',
  },
  editButton:{
    [theme.breakpoints.up('sm')]: {
      marginTop:40,
    }, 
  },
  favoriteButton:{
    top:10,
    right:10,
    transform:'translateY(0)'
  },
});

class ItemComp extends Component{
  constructor(props) {
    super(props);
    // console.log(this.props.item)
    // console.log(Collections.files.find().fetch())    
    let obj=JSON.parse(MyCat),o={},i;
    let cat=this.props.item.categorie;
    let completeCat="";

    for(o in obj){
      if(obj[o]['sub'].length>0){
        for(i=0;i<=obj[o]['sub'].length-1;i++){   
          if(cat === obj[o]['sub'][i]['id'])
            completeCat=obj[o]['text'] + " : " + obj[o]['sub'][i]['text'];
        }
      }
    }
    // console.log(completCat)

    this.state = {
      // ready: handle.ready(),
      completeCat:completeCat,
      item:this.props.item,
      fav:[],
    };
  }

  componentDidMount(){
    // console.log('mount');
    this.renderLink();
    if(Meteor.userId() !== null){
      // console.log(Meteor.userId())
      Meteor.call('favorite',-1,(err,res) => {
        if (err) {
          console.error('error', err)
        }else{
          // console.log(res);
          this.setState({
            fav:res === undefined ? [] : res,
          });
        }
     }); 
    }
  }

  componentDidUpdate(){    

  }

  favorite(e,id){
    e.preventDefault();
    Meteor.call('favorite',id,(err,res) => {
      if (err) {
        console.error('error', err)
      }else{
        // console.log(res);
        this.setState({
          fav:res,
        });
      }
   }); 
  }

  removeAnnonce(e,id){
    e.preventDefault();
      Meteor.call('annonces.remove',id,(err,res) => {
      if (err) {
        console.error('error', err)
      }else{
        // console.log(res);
        // this.setState({
        //   fav:res,
        // });
      }
   });  
  }

  renderLink(){
    Meteor.call('preview.get',this.state.item.photos[0],(err,res) => {
      if (err) {
        console.error('error', err)
      }else{
        // console.log(res);
        this.setState({
          link:res,
        });
      }
   }); 
  }

  render() {  
    let {classes,width,context}=this.props;
    let {item,completeCat,link,fav}=this.state; 
    // console.log(item)
    // console.log(Meteor.userId())
    return (
      <Paper className={classes.paper} elevation={1}>
          <ListItem 
          alignItems="flex-start"
          // component={context!=="myAnnonces" ? Link : 'div'}
          component={Link}
          to={"/annonce/" + item._id } 
          button>
        {
          link ?
            <div className={classes.thumbnail}>
              <img src={link} className={classes.img} />              
            </div>
            : 
            <div className={classes.progressContainer}>
              <CircularProgress className={classes.progress} />              
            </div>
        }
        <ListItemText 
          primary={
            <React.Fragment>
              <Typography component="h4" variant="h6" noWrap>
                {item.title}
              </Typography>
              {
              context!=='myAnnonces' &&
              <React.Fragment>
                <Typography component="p" >
                  {completeCat}
                </Typography>
                <Typography paragraph >
                  {item.city}
                </Typography>
              </React.Fragment>  
              }               

              <Typography component="h4" variant="h6">
                 {item.price +' $'}
              </Typography>

              {context==='myAnnonces' &&
                <Button 
                  variant="outlined"
                  component={Link} 
                  to={"/modifier_une_annonce/" + item._id } 
                  color="primary" 
                  className={`${classes.button} ${classes.editButton}`}>Modifier
                </Button>
              }
            </React.Fragment>
          }/>

          <ListItemSecondaryAction className={classes.favoriteButton} >
           
           { context !== 'myAnnonces' ?
           <div>
             <div>
              <IconButton onClick={(e) => this.favorite(e, item._id)}>
               {
                 fav.indexOf(item._id) === -1 ?
                 <FavoriteBorder color="primary"/> 
               :
                 <Favorite color="primary"/>
               }
               </IconButton>
              </div>

              {context==='favorites' && item.createBy !== Meteor.userId() &&
                <div>
                <IconButton 
                  to={{
                    pathname:"/messages/",
                    state:{
                      objectId:item._id
                    }
                  }}
                  component={Link} 
                  color="primary" >
                  <MessageIcon /> 
                </IconButton>
              </div>}
            </div>
            :
            <IconButton 
              onClick={(e) => this.removeAnnonce(e, item._id)}
              color="secondary" 
              className={classes.button}>
              <Delete /> 
            </IconButton>
          }
          </ListItemSecondaryAction>
        </ListItem>       
      </Paper>
      );
  }
}

ItemComp.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default compose(
    withWidth(),
    withStyles(styles),
)(ItemComp);