import React, { Component } from 'react';
import { withRouter,Link } from 'react-router-dom';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import compose from 'recompose/compose';

import { _app, Collections } from '../../lib/core.js';
import MyCat from '../../api/categorie.js';

import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import withWidth,{ isWidthUp } from '@material-ui/core/withWidth';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import IconButton from '@material-ui/core/IconButton';
import Checkbox from '@material-ui/core/Checkbox';
import FavoriteBorder from '@material-ui/icons/FavoriteBorder';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';


import Favorite from '@material-ui/icons/Favorite';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import MessageIcon from '@material-ui/icons/Forum';

const styles = theme => ({
  root: {
    padding:0,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  paper: {
    marginTop:10,
  },
  thumbnail:{
    maxWidth:80,
    minWidth:80,
    backgroundColor:'gray',  
  },
  img: {
    width: '100%',
  },
  button:{
    textTransform:'none',
  },
  editButton:{
    [theme.breakpoints.up('sm')]: {
      marginTop:40,
    }, 
  },
  favoriteButton:{
    top:10,
    right:10,
    transform:'translateY(0)'
  },
  progressContainer:{
    width:80,
    height:80,
  },
  progress:{
    position:'relative',
    top:20,
    left:20,
  },
});

class ItemMessageComp extends Component{
  constructor(props) {
    super(props);

    this.state = {
      fav:[],
    };
  }

  componentDidMount(){
    this.renderLink();
  }

  componentDidUpdate(){    

  }

  removeDiscussion(e,id){
    e.preventDefault();
      Meteor.call('discussions.remove',id,(err,res) => {
      if (err) {
        console.error('error', err)
      }
   });  
  }

  renderLink(){
    Meteor.call('discussions.getPreview',this.props.item.object,(err,res) => {
      if (err) {
        console.error('error', err)
      }else{
        // console.log(res);
        this.setState({
          link:res,
        });
      }
   }); 
  }

  render() {  
    let {classes,width,item}=this.props;
    let {link}=this.state; 
    // console.log(item)
    return (
      <Paper className={classes.paper} elevation={1}>
          <ListItem 
          alignItems="flex-start"
          component={Link}
          to={{
            pathname:"/messages",
            state:{
              discussionId:item._id
            }
          }}
          button>
        {
          link ?
            <div className={classes.thumbnail}>
              <img src={link} className={classes.img} />              
            </div>
            :
            <div className={classes.progressContainer}>
              <CircularProgress className={classes.progress} />              
            </div>
        }
        <ListItemText 
          primary={
            <React.Fragment>
              <Typography component="h4" variant="h6" noWrap>
                {item.createBy}
              </Typography>

              <React.Fragment>
                <Typography component="p" >
                  {item.object}
                </Typography>
                <Typography paragraph >
                  {item.objectOwner}
                </Typography>
              </React.Fragment>  

            </React.Fragment>
          }/>

          <ListItemSecondaryAction className={classes.favoriteButton} > 
            <IconButton 
              onClick={(e) => this.removeDiscussion(e, item._id)}
              color="secondary" 
              className={classes.button}>
              <Delete /> 
            </IconButton>
          </ListItemSecondaryAction>
        </ListItem>       
      </Paper>
      );
  }
}

ItemMessageComp.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default compose(
    withWidth(),
    withStyles(styles),
)(ItemMessageComp);