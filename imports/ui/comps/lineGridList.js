import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { _app, Collections } from '../../lib/core.js';
import compose from 'recompose/compose';




import withWidth from '@material-ui/core/withWidth';
import { withStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import IconButton from '@material-ui/core/IconButton';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import CircularProgress from '@material-ui/core/CircularProgress';
// import tileData from './tileData';
import CloseIcon from '@material-ui/icons/Close';

const styles = theme => ({
  root: {
    // display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    overflow:'overlay',
    flexWrap: 'nowrap',
    height:204,
    // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
    transform: 'translateZ(0)',
  },
  icon: {
    color: 'white',
  },
  titleBar: {
    background:
      'linear-gradient(to bottom, rgba(0,0,0,0.7) 0%, ' + 'rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
  },
  img: {
    width:'100%',
  },
  divPromptAddPhoto:{
    position:'absolute',
    width:'100%',
    zIndex:1,
    marginTop:80,
    textAlign:'center',
  },
  progress: {
    position:'absolute',
    top: '40%',
    left: '40%',
    zIndex: 1,
  },
});

class SingleLineGridList extends Component {
  constructor(props) {
    super(props);
  }

  componentDidUpdate(){   
  // console.log('update')      
    if(this.props.bAddPhoto){
      // console.log('scroll')
      ReactDOM.findDOMNode(this.refs.gridList).scrollTo(10000,0);
 
      Meteor.setTimeout(() => {
        this.props.resetScrollToEnd();
      }, 500); 
    }        
  }

  render(){
      const { classes } = this.props;
      const { tileData,width } = this.props;
      let cols;

      // console.log(tileData);

      switch (width){
        case 'xs':
            cols=1.5
            break;
        case 'sm':
            cols=2.5
            break;
        case 'md':
            cols=2.5
            break;
        case 'lg':
            cols=3.5
            break;
        case 'xl':
            cols=4.5
            break;
        default:
            cols=2.5
      }


    return (
      <div className={classes.root}>
        {
          tileData.length === 0 ?
          <div className={classes.divPromptAddPhoto}>Ajoutez des photos</div>
          :''
        }

        <GridList ref="gridList" className={classes.gridList} cols={cols} cellHeight={200}>
          {
            tileData.map((tile,key) => (
            <GridListTile key={key}>           
              {
                tile.link === null ? 
                <CircularProgress size={30} className={classes.progress} />
              :
              <div>
                <img  src={tile.link} className={classes.img} />
                <GridListTileBar
                  className={classes.titleBar}                  
                  titlePosition="top"
                  actionIcon={
                    <IconButton onClick={(e) => this.props.rmFile(e,tile.id)}>
                      <CloseIcon className={classes.icon} />
                    </IconButton>
                  }
                />         
              </div>
              }       
            </GridListTile>
          ))
          }

        </GridList>

      </div>
    );
  }
}

SingleLineGridList.propTypes = {
  rmFile: PropTypes.func.isRequired,
  resetScrollToEnd: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
};

export default compose(
  withStyles(styles),
  withWidth(),
)(SingleLineGridList);