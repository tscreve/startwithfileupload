import React, { Component } from 'react';
import { withRouter,Link } from 'react-router-dom';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import ItemComp from './itemComp.js';
import ItemDiscussionComp from './itemDiscussionComp.js';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Checkbox from '@material-ui/core/Checkbox';
import Avatar from '@material-ui/core/Avatar';

const styles = theme => ({
  root: {
    padding:0,
    width: '100%',
    // maxWidth: 360,
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    width: '100%',
  },
  img: {
    width: '100%',
  },
});

class ListComp extends React.Component {
  constructor(props) {
    super(props);
 
    this.state = {
      // hideCompleted: false,
    };
  }

  componentDidUpdate(){    
    
  }

  render() {     
    let {classes,items,context}=this.props;
    // console.log('items : ',items);

     return (
      items && context==='discussions' ?
          <List className={classes.root}>
            {items.map((item,key) => (
                <ItemDiscussionComp item={item} key={item._id}/>
            ))}
          </List>
      : items ?
          <List className={classes.root}>
            {items.map((item,key) => (
                <ItemComp item={item} key={item._id} context={context}/>
            ))}
          </List>
      :''
    );
  }
}

ListComp.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ListComp);