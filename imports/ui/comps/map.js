import { Meteor } from 'meteor/meteor';
import React, {createRef, Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import compose from 'recompose/compose';
import { withStyles } from '@material-ui/core/styles';

import L from 'leaflet';
import { MapControl, ZoomControl,Circle,  CircleMarker,  Map,  Marker,  Polygon,  Popup,  Rectangle,  TileLayer,Tooltip,VideoOverlay,withLeaflet } from 'react-leaflet';

import withWidth,{ isWidthUp } from '@material-ui/core/withWidth';
import Button from '@material-ui/core/Button';
import Fab from '@material-ui/core/Fab';


type Position = { lat: number, lng: number }

type State = {
  hasLocation: boolean,
  center: Position,
  marker: Position,
  zoom: number,
};

const styles = theme => ({

});

class MyMap extends Component {
  constructor(props) {
    super(props);

    this.state = {
        center: this.props.latlng && this.props.latlng.lat ? this.props.latlng : {
          lat: 48.9,
          lng:2.34,
        },
        marker: this.props.latlng && this.props.latlng.lat ? this.props.latlng : {
          lat: 48.9,
          lng: 2.34,
        },
        zoom: 16,
        hasLocation: false,
        address:this.props.address,
      };
    // console.log('constr')
  }
mapRef = createRef()
refmarker = React.createRef()
 
componentDidMount(){       
  // console.log('mount')
  // console.log('props',this.props.latlng)
  // console.log('marker',this.state.marker)
}

componentDidUpdate(){      
  // console.log(this.state.address)
  // console.log(this.props.address)
  if(this.props.address === 'MyPos' && this.state.address !== this.props.address && this.state.address !== null){
    let map = this.mapRef.current
    if (map != null) {
      console.log('locate')
      map.leafletElement.locate()
      return
    }
  }
  if(this.props.latlng && this.props.latlng.lat && this.state.marker !== this.props.latlng){
    this.setState({
      marker:this.props.latlng,
      center:this.props.latlng,
    });
  }
  if(this.props.search && this.props.address && this.state.address !== this.props.address){
    let r,latlng=[],placeName=[],postcode=[];
    let map = this.mapRef.current;
    let addr=this.props.address;
    let self=this;

    if (map != null) {
      // map.leafletElement.locate()
      // console.log(addr)

      Meteor.call('GeoCode', addr, function (err, res) {
        if (err){
          console.log(err);
        }
        // console.log(res)

        if(res){
          self.setState({
            address: self.props.address,
            marker: res.latlng,
            center:  res.latlng,
          })
          self.props.returnLatLng(res,'')
        }
      });
    }
  }
}

onMapClick = (e) =>{
  let placeName="";
  let map = this.mapRef.current;  
  let zoom=map.leafletElement.getZoom()
  let self=this

  if (map != null) {
    if(!this.props.clickable){
      console.log(this.props.clickable)
      this.props.onClick()
    }else{
      Meteor.call('GeoCode', e.latlng.lat + "," + e.latlng.lng, function (err, res) {
        if (err){
          console.log(err);
        }
        console.log(res)
        self.setState({
          marker: res.latlng,
          // address: '',
          zoom: zoom,
        })  
        self.props.returnLatLng(res,'')
      }); 
    }
  }
}

handleLocationFound = (e: Object) => {
  let self=this  
  Meteor.call('GeoCode', e.latlng.lat + "," + e.latlng.lng, function (err, res) {
    if (err){
      console.log(err);
    }
    // console.log(res)
    self.setState({
      marker: res.latlng,
      center: res.latlng,
      hasLocation: true,
      address:'MyPos',
    })  
    self.props.returnLatLng(res,'MyPos')
  }); 
}

  render() {
    const {classes,width} =  this.props;
    const position =  [this.state.center.lat, this.state.center.lng]    
    const markerPosition = [this.state.marker.lat, this.state.marker.lng]
    // console.log(markerPosition)

    return (
    <div 
        style={!isWidthUp('md',width) ? {height:innerHeight-56} : {height:'100%'}}
        >
    <Map 
        center={position} 
        length={4}
        onClick={this.onMapClick}        
        onLocationfound={this.handleLocationFound}
        ref={this.mapRef}
        zoom={this.state.zoom}
        >
        <TileLayer
          attribution="&amp;copy <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        <Marker
          position={markerPosition}
          ref={this.refmarker}>
        </Marker>             
      </Map>
     </div>
    );
  }
}

MyMap.propTypes = {
  // returnLatLng: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
};

export default compose(
  withWidth(),
  withStyles(styles),
)(MyMap);