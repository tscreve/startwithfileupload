import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import { withRouter,Link } from 'react-router-dom';
import ReactDOM from 'react-dom';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import compose from 'recompose/compose';
import { Session } from 'meteor/session';

import MyCat from '../../api/categorie.js';
import MyRegion from '../../api/region.js';
import SelectCat from './selectCatComp.js';
import SelectRegion from './selectRegionComp.js';
import ListComp from './listComp.js';

import withWidth,{ isWidthUp } from '@material-ui/core/withWidth';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import Divider from '@material-ui/core/Divider';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import Card from '@material-ui/core/Card';
import Fab from '@material-ui/core/Fab';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Slide from '@material-ui/core/Slide';
import Typography from '@material-ui/core/Typography';

import SearchIcon from '@material-ui/icons/Search';
import ExpandMore from '@material-ui/icons/ExpandMore';
import ExpandLess from '@material-ui/icons/ExpandLess';


const AnnoncesQuery = new Mongo.Collection(null);

const styles = theme => ({
  root: {
    flexGrow: 1,
    display:'flex',
  },
  container:{
    marginTop:0,
    [theme.breakpoints.down('xs')]: {
      marginTop:10,
    },
  },
  hideOnSmall:{
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
    // [theme.breakpoints.up('md')]: {
    //  display: 'block',
    // },
    // [theme.breakpoints.up('lg')]: {
    //  display: 'block',
    // },
  },
  path:{
    marginLeft:20,
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width:'98%',
    fontSize:21,
    [theme.breakpoints.down('xs')]: {
      fontSize:18,
    },
  },
  searchBar: {
    padding: 5,
    alignItems: 'center', 
    width:'98%',
    margin:'auto', 
  },
  subBar:{
    [theme.breakpoints.down('xs')]: {
      padding:0,
    },
    padding:'3px 0 3px 0',
    backgroundColor:'white',
  },
  iconButton: {
    padding: 10,
    marginLeft :15,
    marginRight :15,
    [theme.breakpoints.down('xs')]: {
    margin :0,
    },
  },
  moreSearchButton:{
    [theme.breakpoints.down('xs')]: {
    display :'none',
    },   
  },
  buttonDivider: {
    width: 1,
    height: 56,
    [theme.breakpoints.down('xs')]: {
      // display:'none',
    },
  },
  divider: {
    width: '100%',
    marginTop:10,
    marginBottom:15,
  },
  // fab: {
  //   position: 'fixed',
  //   bottom: theme.spacing.unit * 2,
  //   right: theme.spacing.unit * 2,
  // },
});

class VueListComp extends Component {
  constructor(props) {
    super(props);
    // console.log(Session.get('region'));
    // console.log(this.props.cat);
    let queryData={},cat
    let mainCat,newCat,region="All";

    console.log(Session.get('search'))
    console.log(this.props.type)
    console.log(this.props.cat)
    console.log(this.props.region)

    queryData.type=this.props.type;

    if(this.props.cat){
      cat = this.props.cat.split(",")
      mainCat=cat[0];
      newCat=cat.splice(1,cat.length).toString()      
    }else{
      mainCat = newCat = "All"
    }
    queryData.categorie=newCat;

    if(this.props.region){
      region=this.props.region;
      Session.set('region',region);
    }else if(Session.get('region')){
      region=Session.get('region');
    }
    queryData.region=region;

    // && !this.props.cat && !this.props.region
    if(Session.get('search')  && Session.get('search').type===this.props.type && Session.get('search').categorie===newCat){
      console.log(Session.get('search').categorie)  
      console.log(this.props.cat)  
      queryData=Session.get('search')
      // console.log(queryData)
    }

    this.state = {   
      searchDrawer:false, 
      subBar:false,
      cat:this.props.cat,
      curPage:0,
      nbRows:50,
      totalRes:0,
      queryData:queryData,
      moreSearch:false,
      mainCat:mainCat,
      brCrCat:{
        type:'',
        current:[],
        main:[],
        sub:[],                       
      },
    };  

    this.getScroll=this.getScroll.bind(this) 
  }

  getScroll = (e) => {   
    e.preventDefault(); 
    const subBar=window.scrollY > 250 ? true : false;
    if(scrollY!==0){
      Session.set('scroll',history.state.key + ',' + scrollY); 
    }
    this.setState({subBar:subBar})
  }

 restoreScroll(){
    let a=Session.get('scroll') ? Session.get('scroll').split(',') :[]
    // console.log(a)
    if(history.state.key === a[0] && Number(a[1])>30){
      window.scrollTo(0, a[1]);
    }else{
      window.scrollTo(0, 0);
    }
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.getScroll,false);
  }
  componentDidMount(){
    window.addEventListener("scroll", this.getScroll,false);
    this.getAnnonces();
  }

  componentWillUpdate(prevProps) {
   
  }

  componentDidUpdate(prevProps){    
    const { type,history,cat } = this.props;
    const { queryData } = this.state;

    // console.log(prevProps)
    // console.log(this.props)
    console.log(Session.get('search')) 
    // console.log(queryData) 

    if(prevProps.type !== type){ 
      queryData.type=type; 
      queryData.categorie='All'
      queryData.searchText=''
      Session.set('search',null)
      this.setState({cat:'All',queryData:queryData,curPage:0}); 
      this.getAnnonces(); 
    }else if(prevProps.cat!==cat){      
      let cat1=""
      if(cat){
        cat1=cat.split(',').splice(1,cat.length).toString()
      }else{
        cat1='All'
      }   

      queryData.categorie=cat1
      this.setState({cat:cat,queryData:queryData,curPage:0}); 
      this.getAnnonces(); 
    } 
    // console.log(this.state.curPage); 
  }

  getAnnonces(){
    const { curPage,nbRows,queryData } = this.state;   
     // console.log(queryData)
     Meteor.call('annonces.get',{ curPage,nbRows,queryData },(err,res) => {
        if (err) {
          console.error('error', err)
        }else{
          AnnoncesQuery.remove({});
          // console.log('res', res);
          res['annonces'].forEach((item) => {
            // console.log(ObjectId(item._id));
            AnnoncesQuery.insert(item);
          });
          // console.log(AnnoncesQuery.find().fetch());
          let ann=AnnoncesQuery.find().fetch();
          // console.log(ann);
          this.setState({
            brCrCat:{
              type:queryData.type,
              current:res.cat.currentCat,
              main:res.cat.mainCat, 
              sub:res.cat.subCat,  
              withSub:res.cat.withSub, 
            },
            annonces:ann,
            totalRes:res['count'],
          });
            this.restoreScroll()
        }

     });
  }

  loadMore(e){
    e.preventDefault();
    this.setState({
      curPage:this.state.curPage+=1,
    });
     this.getAnnonces();
    // console.log(totalRes);
  }

  submit(e){
    e.preventDefault();
    const { history,cat,type } = this.props; 
    const { mainCat,queryData } = this.state; 

    console.log(queryData)

    if(cat !== queryData.categorie){
      history.push('/' + (type === 'Offre' ? 'offres' : 'demandes') + '/categorie/' + mainCat + ',' + queryData.categorie); 
      // history.push('/demandes/categorie/' + mainCat + ',' + queryData.categorie); 
      // history.push('/annonces/categorie/' + mainCat + ',' + queryData.categorie); 
      this.getAnnonces(); 
    }else{
      this.getAnnonces();  
    }
     Session.set('search',queryData);
    Session.set('scroll',null);
    this.setState({
      moreSearch:false,
      searchDrawer:false,      
    });
  }

  returnCat(e){
    const {queryData} = this.state;  
    let cat=e.target.value.split(",")
    let mainCat=cat[0];
    let newCat=cat.splice(1,cat.length) 
    // console.log(mainCat,newCat.toString())
    queryData.categorie=newCat.toString();
    this.setState({queryData:queryData,mainCat:mainCat});
  }

  returnRegion(e){
    const {queryData} = this.state;  
    console.log(e.target.value); 
    queryData.returnPlace=e.target.value;
    this.setState({queryData:queryData});
  }

  handleChangeInputSearch(e){
    const {queryData} = this.state;  
    // console.log(e.target.value)

    queryData.searchText=e.target.value;
    this.setState({queryData:queryData});
  }

   moreSearch(e){    
    e.preventDefault();
    // console.log(this.state.mainCat,this.state.queryData.categorie)
    this.setState({moreSearch:!this.state.moreSearch});
  }

  toggleSearchDrawer(e){
    e.preventDefault();
    const {searchDrawer} = this.state; 
    this.setState({searchDrawer:!searchDrawer});
  }

  render() {
    const { classes,width } = this.props;
    const { curPage,nbRows,annonces,totalRes,listCat,queryData,
            moreSearch,mainCat,brCrCat,searchDrawer,subBar} = this.state;
    
    let curPlace=queryData.returnPlace ? queryData.returnPlace : queryData.region;
    // console.log(queryData)
    // let curPlace=queryData.region;

    // console.log(brCrCat)
    // console.log('Region : ',queryData.region,queryData.returnPlace);
    // console.log('Categorie : ',queryData.categorie);
    // console.log(queryData);
    // console.log(mainCat,queryData.categorie)
    console.log('Page : ',curPage);
    console.log('Resultats par page : ',nbRows);
    console.log('Total des resultats : ',totalRes);
    console.log('Resultats a afficher : ',annonces);

    const form=<form 
                    style={{width:'100%'}}
                    noValidate 
                    autoComplete="off">
                      <Paper className={classes.searchBar} elevation={subBar ? 0 : 1}>
                        <Grid container className={classes.root} >
      
                          <Grid item xs={12} sm={12} style={{display:'flex'}}>
                            <InputBase 
                              value={queryData.searchText}
                              className={classes.textField}
                              placeholder="Que cherchez vous ?"
                              id="inputSearch" 
                              onChange={(e) => this.handleChangeInputSearch(e)} 
                              />
                            <IconButton  className={classes.iconButton} aria-label="Directions">
                              <SearchIcon color="primary" fontSize="large" onClick={(e) => this.submit(e)}/>
                            </IconButton>
                            <Divider className={classes.buttonDivider}/>
                            <IconButton 
                              className={classes.iconButton}  
                              aria-label="Search"
                              color="secondary" onClick={(e) => this.moreSearch(e)}>
                              {moreSearch===true && <ExpandLess fontSize="large"/>}                                
                              {moreSearch===false && <ExpandMore fontSize="large"/>}
                            </IconButton>    

                          </Grid>

                          {////////////////      MORE SEARCH       /////////
                          }
                          <div style={{width:'100%',display:moreSearch ? 'block' : 'none'}}>                    
                            <Divider className={classes.divider}/>
                            <Grid item xs={12} sm={12} style={{display:'flex'}}>
                            <SelectCat 
                              style={{marginRight:15}}
                              view={"full"} 
                              curCat={mainCat + "," + queryData.categorie} 
                              handleChange={(e) => this.returnCat(e)}/>
                            <SelectRegion                     
                              curRegion={queryData.region} 
                              selected={curPlace}
                              handleChange={(e) => this.returnRegion(e)}/>
                            </Grid> 
                          </div>
                        </Grid>
                      </Paper>
                  </form>




    return (
        <div>   
            {subBar && !isWidthUp('md',width) &&
                  <Slide direction="down" in={true} mountOnEnter unmountOnExit>
                    <AppBar position="fixed" className={classes.subBar} >
                      <Toolbar  className={classes.subBar}>
                        <Grid container>
                          <Grid item md={2} className={classes.hideOnSmall}>
                          </Grid>
            
                          <Grid item xs={12} sm={12} md={8} style={{display:'flex'}}>
                            {form}
                          </Grid>
            
                          <Grid item  md={2} className={classes.hideOnSmall}>
                          </Grid>
                        </Grid>
                       </Toolbar>
                      </AppBar>
                    </Slide>}


            {isWidthUp('sm',width) &&
              <p className={classes.path}>
               <Link to={'/'+brCrCat.type}>{brCrCat.type}{brCrCat.main === '' ? '' : ' > '}</Link>
               {
                 brCrCat.type === 'Offre' ?
                 <span>
                   <Link to={'/offres/categorie/'+brCrCat.main[0]+','+brCrCat.sub}>{brCrCat.main[1]}</Link>
                   {brCrCat.withSub===true &&
                       <Link to={'/offres/categorie/'+brCrCat.main[0]+','+brCrCat.current[0]}> > {brCrCat.current[1]}</Link>
                   }
                 </span>
               :
                 <span>
                   <Link to={'/demandes/categorie/'+brCrCat.main[0]+','+brCrCat.sub}>{brCrCat.main[1]}</Link>
                   {brCrCat.withSub===true &&
                      <Link to={'/demandes/categorie/'+brCrCat.main[0]+','+brCrCat.current[0]}> > {brCrCat.current[1]}</Link>
                   }
                 </span>
               }
             </p>} 

            {
            //   isWidthUp('xs',width) ?
            // <Drawer
            //   anchor="bottom"
            //   open={searchDrawer}
            //   onOpen={(e) => this.toggleSearchDrawer(e)}
            //   onClose={(e) => this.toggleSearchDrawer(e)}
            // >
            //     <div
            //       tabIndex={0}
            //       role="button"               
            //     >
            //      {form}            
            //     </div>
            //   </Drawer>
            //   :
            <div className={classes.container}>
              {form}
              <ListComp items={annonces} />
            </div>

            // <Button variant="contained" onClick={(e) => this.loadMore(e)}>Load more...</Button>




            // isWidthUp('xs',width) &&
            //       <Fab  
            //         onClick={(e) => this.toggleSearchDrawer(e)}                  
            //         className={classes.fab}
            //         color="primary">
            //       <SearchIcon fontSize="large"/>
            //     </Fab>
              }
      </div>
    );
  } 
}

VueListComp.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default compose(
  withWidth(),
  withStyles(styles),
)(withRouter(VueListComp));
