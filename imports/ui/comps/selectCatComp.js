import React, { Component } from 'react';
import { withRouter,Link } from 'react-router-dom';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import compose from 'recompose/compose';
import { withStyles } from '@material-ui/core/styles';
import withWidth,{ isWidthUp } from '@material-ui/core/withWidth';

import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import InputBase from '@material-ui/core/InputBase';



import MyCat from '../../api/categorie.js';
import MyCatDiv from './DivSelectCatComp.js'


const styles = theme => ({
  hideOnSmall:{
    [theme.breakpoints.down('xs')]: {
      display: 'none',
    },
    // [theme.breakpoints.up('md')]: {
    //  display: 'block',
    // },
    // [theme.breakpoints.up('lg')]: {
    //  display: 'block',
    // },
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  selectContainer:{
    minWidth:180,
    marginRight:15,  
    // backgroundColor:theme.palette.primary.main,  
  },
  catDiv:{
    position:'absolute',
    top:'100px',
    width:'97%',
    // marginLeft:'2%',
  },
  show:{
    display:'block',
  },
  hide:{
    display:'none',
  },
  textField: {
    width: '100%',
  },
});

class selectCat extends Component {
  constructor(props) {
    super(props);

    let i,cat=[],subCat,temp=[],o;
    let obj=JSON.parse(MyCat);

    for(o in obj){

      for(i=0;i<=obj[o]['sub'].length-1;i++){        
          temp.push([obj[o]['sub'][i]['id'],obj[o]['sub'][i]['text'],obj[o]['id']]);
          subCat +=  (obj[o]['sub'][i]['id']) + "," ;
          // console.log(obj[o]['id'])
          // console.log(obj[o]['sub'][i]['text']);
      }
      subCat = subCat ? subCat.substr(0,subCat.length-1) : ''  
      let v = (obj[o]===obj[0]) ? "All" : subCat
       // console.log(subCat)
      cat.push([v,"-- " + obj[o]['text'] + " --", obj[o]['id']]);
      // console.log(obj[o]['text']);
      cat = cat.concat(temp)
      // console.log(cat)
      subCat=[]
      temp=[]
    } 

    this.state = { 
      listCat:cat,
      curCat:this.props.curCat,
      displayCatDiv:false,
    };
  }


  componentDidUpdate(){
    // console.log(this.state.curCat)
    // console.log(this.props.curCat)
    if(this.props.curCat !== this.state.curCat){
      this.setState({curCat:this.props.curCat});
    }
  }

  componentDidMount(){

  }

  componentWillUnmount() {

  }

  hideDiv(e){
     e.preventDefault();
      this.setState({
        displayCatDiv: false,
      });
  }

  handleClick(e){
    if(isWidthUp('sm',this.props.width)){
      this.setState({
        displayCatDiv: !this.state.displayCatDiv,
      });
    }    
  }

  render() {
    const { classes,width} = this.props;
    const {listCat,curCat,view,displayCatDiv} = this.state;
    // console.log(curCat)
     return (
      <div 
        className={classes.selectContainer} >
      {
       displayCatDiv ?
        <div className={`${classes.hideOnSmall} ${classes.catDiv}`}>
        <MyCatDiv 
          context="search"
          curCat={curCat}
          hideDiv={(e) => this.hideDiv(e)} 
          onChange={(e) => this.props.handleChange(e)}
          />
        </div> : ''
      }



        <TextField
          style={{width:'100%'}}
          variant="filled"
          onChange={(e) => this.props.handleChange(e)}
          onClick={(e) => this.handleClick(e)}
          value={curCat}             
          id="catSelect"                
          select
          label="Categorie"
          disabled={isWidthUp('sm',width)  ? true : false}
        >
          {
            listCat.map((c,key )=> (                        
            <MenuItem                   
              key={key}
              value={c[2] + "," + c[0]}>
              {c[1]}
            </MenuItem>
          ))
          }
        </TextField>
      </div>
    );
  }
}

selectCat.propTypes = {
  handleChange: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
};

export default compose(
  withWidth(),
  withStyles(styles),
)(selectCat);