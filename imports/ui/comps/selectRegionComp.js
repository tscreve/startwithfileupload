import React, { Component } from 'react';
import { withRouter,Link } from 'react-router-dom';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';

import MyRegion from '../../api/region.js';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  selectContainer:{
    minWidth:100, 
    // backgroundColor:theme.palette.primary.light,  
  },
  textField: {
    width: '100%',
  },
});

class selectRegion extends Component {
  constructor(props) {
    super(props);

    let curRegion=this.props.curRegion;

    let i,region=[],o;
    let obj=JSON.parse(MyRegion);

    // AUTOUR DE MOI
    region.push(["ArroundMe","Autour de moi"]);  
    for(o in obj){
      if(obj[o]['id'] === curRegion){
        // REGION SELECTIONNEE
        region.push([obj[o]['id'],obj[o]['text']]);
        // REGION VOISINES
        if(obj[o]['dept'].length > 1 && obj[o]['id'] !== "corse"){
          region.push(["ArroundRegion","Regions voisines"]);
        }
        // TOUTE LA FRANCE
        if(obj[o]['id'] !== 'All'){
          region.push([obj[0]['id'],obj[0]['text']]);
        }
        // SEPARATEUR DEPARTEMENTS
        if(obj[o]['dept'].length > 1){
          region.push(["0","-- DEPARTEMENTS --"]);
          // DEPARTEMENTS
          for(i=0;i<=obj[o]['dept'].length-1;i++){
              region.push(["dept_" + obj[o]['dept'][i]['id'],obj[o]['dept'][i]['text']]);
              // console.log(obj[o]['dept'][i]['text']);
          }
        } 
      }
    }  
    // console.log(region); 

    this.state = { 
      listRegion:region,
      curRegion:curRegion,
    };
  }
  componentDidUpdate(){    
    if(this.props.curRegion !== this.state.curRegion){
      this.setState({curRegion:this.props.curRegion});
    }
  }

  render() {
    const {classes,selected} = this.props;
    const {listRegion,curRegion} = this.state;

    // console.log(selected)

     return (
      <div className={classes.selectContainer}>
        <TextField
          style={{width:'100%'}}
          variant="filled"
          onChange={(e) => this.props.handleChange(e)}
          value={selected}               
          id="regionSelect"                
          select
          label="Region"
        >
          {listRegion.map((c,key )=> (                     
            <MenuItem   
              disabled={c[1].startsWith("--") ? true : false}               
              key={key}
              value={c[0]}>
              {c[1]}
            </MenuItem>
          ))}
        </TextField>
        </div>
    );
  }
}

selectRegion.propTypes = {
  handleChange: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(selectRegion);

