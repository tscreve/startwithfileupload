import { Meteor } from 'meteor/meteor';
import React, {createRef, Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { withStyles } from '@material-ui/core/styles';

// import L from 'leaflet';
import {Map,  Marker, TileLayer } from 'react-leaflet';

import Button from '@material-ui/core/Button';

const styles = theme => ({
  root: {
    // marginTop: 50,
    height:'100%',
  },
});

class SimpleMap extends Component {
  constructor(props) {
    super(props);

    // console.log(this.props)
    this.state = {
        zoom: 14,
      };
   
  }
 
componentDidMount(){       
}

componentDidUpdate(){      
   // console.log(this.props)
}

// onMapClick = (e) =>{

// }



  render() {
    const {classes} =  this.props;
    const position =  this.props.latlng     
    // console.log(markerPosition)

    return (
    <div className={classes.root}>
    <Map 
        center={position} 
        length={4}
        zoom={this.state.zoom}
        >
        <TileLayer
          attribution="&amp;copy <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        <Marker
          position={position}>
        </Marker>             
      </Map>
     </div>
    );
  }
}

SimpleMap.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SimpleMap);