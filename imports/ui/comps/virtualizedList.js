import Immutable from 'immutable';
import PropTypes from 'prop-types';
import React, { Component } from 'react';

import {AutoSizer,List,CellMeasurer, CellMeasurerCache} from 'react-virtualized';

import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
  List:{
    width: '100%',
    // border: '1px solid #DDD',
  },
  row: {
    // height: '100%',
    textAlign: 'left',
    display: 'flex',
    flex:1,
    // flexDirection: 'row',
    alignItems: 'center',
    // padding: '0 25px',
    // backgroundColor: '#fff',
    borderBottom: '1px solid #e0e0e0',
  },
  date:{
    margin:'20px 20px 0 20px',
  },
  myDate:{
    textAlign:'right',
  },
  message:{
    width:'fit-content',
    maxWidth:'75%', 
    padding:10,
    margin:'0 15px 0 15px',
    borderRadius:15,
  },
  myMessage:{
    backgroundColor:'limegreen',
    color:'white',
    float:'right',
  },
  responseMessage:{
    backgroundColor:'lightgray',
  },
  // name:{
  //   fontWeight: 'bold',
  //   marginBottom: 2,
  // },
  // index:{
  //   color: '#37474f',
  // },
  // noRows:{
  //   position: 'absolute',
  //   top: 0,
  //   bottom: 0,
  //   left: 0,
  //   right: 0,
  //   display: 'flex',
  //   alignItems: 'center',
  //   justifyContent: 'center',
  //   color: '#bdbdbd',
  // },
});

class ListMessages extends React.PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      listHeight: innerHeight-64-61,
      overscanRowCount: 10,
      rowCount: this.props.items.length,
      scrollToIndex: this.props.items.length-1,
    };

    this.cache = new CellMeasurerCache({
      minHeight: 80,
      fixedWidth: true
    });

    this.resize=this.resize.bind(this)
    this._rowRenderer = this._rowRenderer.bind(this);
  }

  // listRef = React.createRef()

  resize(){
    this.setState({ listHeight:innerHeight-64-61 });
  }

  componentDidMount() {
    window.addEventListener("resize", this.resize,false);
  }

  componentWillUnmount() {
    // console.log('unmount')
    window.removeEventListener("resize", this.resize,false);
  }

  componentDidUpdate(prevProps){
    // console.log(this.refs)
    // console.log(this.props.items[0].text)
    // console.log('update')

    this.setState({  
      rowCount:this.props.items.length,
      scrollToIndex:this.props.items.length-1,
    });
  }

  render() {
    const {
      listHeight,
      listRowHeight,
      overscanRowCount,
      rowCount,
      scrollToIndex,
      showScrollingPlaceholder,
    } = this.state;

    const{classes}=this.props;

    return (
          <AutoSizer>
            {({width}) => (
              <List
                deferredMeasurementCache={this.cache}
                // ref={(ref) => this.listRef = ref}
                className={classes.List}
                height={listHeight}
                overscanRowCount={overscanRowCount}
                // noRowsRenderer={this._noRowsRenderer}
                rowCount={rowCount}
                rowHeight={this.cache.rowHeight}
                rowRenderer={this._rowRenderer}
                scrollToIndex={scrollToIndex}
                width={width}
              />
            )}
          </AutoSizer>
    );
  }

  _getDatum(index) {
    const {items} = this.props;
    // return items.get(index % items.size);
    return items[index];
  }

  _rowRenderer({index, key,parent, style}) {
    const {classes}=this.props
    const datum = this._getDatum(index);
    // console.log(datum)

    return (
      <CellMeasurer
        cache={this.cache}
        columnIndex={0}
        key={key}
        parent={parent}
        rowIndex={index}
      >
          <div key={key} style={style}>
            <div>
              <div 
                className={`${classes.date} ${datum.author===Meteor.userId() && classes.myDate}`}
                >
                
              </div>
              <div 
                className={`${classes.message} ${datum.author===Meteor.userId() ? classes.myMessage : classes.responseMessage}`}
                >
                {datum.text}
              </div>
            </div>
          </div>

      </CellMeasurer>
    );
  }
}

export default withStyles(styles)(ListMessages);