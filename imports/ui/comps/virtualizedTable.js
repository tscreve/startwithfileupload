import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { isWidthUp } from '@material-ui/core/withWidth';
import { withStyles } from '@material-ui/core/styles';
import TableCell from '@material-ui/core/TableCell';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import { AutoSizer, Column, SortDirection, Table,CellMeasurer, CellMeasurerCache, } from 'react-virtualized';

const styles = theme => ({
  table: {
    fontFamily: theme.typography.fontFamily,
  },
  flexContainer: {
    display: 'flex',
    alignItems: 'center',
    boxSizing: 'border-box',
  },
  tableRow: {
    cursor: 'pointer',
    borderBottom:'solid 1px grey',
  },
  tableRowHover: {
    '&:hover': {
      backgroundColor: theme.palette.grey[200],
    },
  },
  tableCell: {
    flex: 1,
    border:0,
  },
  noClick: {
    cursor: 'initial',
  },
});

  const cache = new CellMeasurerCache({
    fixedWidth: true,
    minHeight: 50,    
  });

class MuiVirtualizedTable extends React.PureComponent {
  getRowClassName = ({ index }) => {
    const { classes, rowClassName, onRowClick } = this.props;

    return classNames(classes.tableRow, classes.flexContainer, rowClassName, {
      [classes.tableRowHover]: index !== -1 && onRowClick != null,
    });
  };

  colCellRenderer = ({ parent,rowIndex,cellData, dataKey,columnIndex = null }) => {
    const { columns, classes, rowHeight, onRowClick,selected } = this.props;

    return (
      <CellMeasurer
        columnIndex={0}
        cache={cache}
        parent={parent}
        rowIndex={rowIndex}
        className={classNames(classes.tableCell, classes.flexContainer, {
          [classes.noClick]: onRowClick == null,
        })}
        variant="body"
        align={(columnIndex != null && columns[columnIndex].numeric) || false ? 'right' : 'left'}
      >
      <div>
        {cellData}
     </div>
      </CellMeasurer>
    );
  };

  cellRenderer = ({ rowIndex,cellData, dataKey,columnIndex = null }) => {
    const { columns, classes, rowHeight, onRowClick,selected } = this.props;

    return (
      <TableCell
        component="div"
        className={classNames(classes.tableCell, classes.flexContainer, {
          [classes.noClick]: onRowClick == null,
        })}
        variant="body"
        style={{height: rowHeight  }}
        align={(columnIndex != null && columns[columnIndex].numeric) || false ? 'right' : 'left'}
      >
        {columns[columnIndex].checkBox ? 
          <Checkbox checked={selected.indexOf(rowIndex) !== -1 || selected[0] === "All" ? true : false} /> 
        : 
        cellData}
      </TableCell>
    );
  };

  headerRenderer = ({ label, columnIndex, dataKey, sortBy, sortDirection }) => {
    const { headerHeight, columns, classes, sort,selected } = this.props;
    const direction = {
      [SortDirection.ASC]: 'asc',
      [SortDirection.DESC]: 'desc',
    };

    const inner =
      !columns[columnIndex].disableSort && sort != null ? (
        <TableSortLabel active={dataKey === sortBy} direction={direction[sortDirection]}>
          {label}
        </TableSortLabel>
      ) : (
        label
      );

    return (
      <TableCell
        component="div"
        className={classNames(classes.tableCell, classes.flexContainer, classes.noClick)}
        variant="head"
        style={{ height: headerHeight }}
        align={columns[columnIndex].numeric || false ? 'right' : 'left'}
      >
        {columns[columnIndex].checkBox ? <Checkbox checked={selected[0] === "All" ? true : false} padding="checkbox"/> : inner}
      </TableCell>
    );
  };

  render() {
    const { classes, columns, ...tableProps } = this.props;

    return (
      <AutoSizer>
        {({ height, width }) => (
          <Table
            className={classes.table}
            height={height}
            width={width}
            {...tableProps}
            rowClassName={this.getRowClassName}
          >
            {columns.map(({ cellContentRenderer = null, className, dataKey, ...other }, index) => {
              let renderer;
              if (cellContentRenderer != null) {
                renderer = cellRendererProps =>
                  this.cellRenderer({
                    cellData: cellContentRenderer(cellRendererProps),
                    columnIndex: index,

                  });
              } else {
                renderer = this.cellRenderer;
              }

              return (
                dataKey==='emails[0]' ?
                <Column
                  key={dataKey}
                  headerRenderer={headerProps =>
                    this.headerRenderer({
                      ...headerProps,
                      columnIndex: index,
                    })
                  }
                  className={classNames(classes.flexContainer, className)}
                  cellRenderer={renderer}
                  dataKey={dataKey}
                  cellDataGetter={({rowData}) => rowData.emails[0].address}
                  {...other}
                /> 
                : dataKey === 'description' ?
                <Column
                  key={dataKey}
                  headerRenderer={headerProps =>
                    this.headerRenderer({
                      ...headerProps,
                      columnIndex: index,
                    })
                  }
                  className={classNames(classes.flexContainer, className)}
                  cellRenderer={this.colCellRenderer}
                  dataKey={dataKey}
                  {...other}
                />
                :
                <Column
                  key={dataKey}
                  headerRenderer={headerProps =>
                    this.headerRenderer({
                      ...headerProps,
                      columnIndex: index,
                    })
                  }
                  className={classNames(classes.flexContainer, className)}
                  cellRenderer={renderer}
                  dataKey={dataKey}
                  {...other}
                />
              );
            })}
          </Table>
        )}
      </AutoSizer>
    );
  }
}

MuiVirtualizedTable.propTypes = {
  classes: PropTypes.object.isRequired,
  columns: PropTypes.arrayOf(
    PropTypes.shape({
      cellContentRenderer: PropTypes.func,
      dataKey: PropTypes.string.isRequired,
      width: PropTypes.number.isRequired,
    }),
  ).isRequired,
  headerHeight: PropTypes.number,
  onRowClick: PropTypes.func,
  selected: PropTypes.array,
  rowClassName: PropTypes.string,
  rowHeight: PropTypes.oneOfType([PropTypes.number, PropTypes.func]),
  sort: PropTypes.func,
};

MuiVirtualizedTable.defaultProps = {
  // headerHeight: 56,
};

const WrappedVirtualizedTable = withStyles(styles)(MuiVirtualizedTable);

class ReactVirtualizedTable extends Component {
  constructor(props) {
    super(props);

    console.log(this.props.data)

    this.state = {
      selected:[],
      rows:this.props.data,
    };    
  }

  componentDidMount(){
    // console.log(this.props.data)
  }

  componentDidUpdate(prevProps){
    // console.log(this.state.selected)
  }

  handleCheckClick(event)  {    
    const { selected } = this.state;
    let newSelect = selected;
    const id=event.index;
    const selectedIndex = selected.indexOf(id);
    // console.log(selected)

    if(event.dataKey === 'select'){
      if(selected.indexOf('All') !== -1){
        newSelect=[];
      }else{
        newSelect=['All'];
      }
    }else{
      // console.log(selected.indexOf('All'))
      if(selected.indexOf('All') !== -1){
        newSelect.splice(selected.indexOf('All'),1)
      }
      if (selectedIndex !== -1) {
        newSelect.splice(selected.indexOf(id),1);
      } else  {
        newSelect.push(id);
      }
    }

    this.setState({ 
      selected: newSelect,
    });
  }

  render(){
  const {selected,rows} = this.state;
  const {context,width,fullHeight} = this.props;

  let col=[{
          width: 100,
          label: 'Select',
          dataKey: 'select',
          checkBox: true,
        }]

  switch(context){
    case 'ann':
    col.push(
            {
              width: 120,
              flexGrow: 1.0,
              label: 'Type',
              dataKey: 'type',
            },
            {
              width: 120,
              flexGrow: 1.0,
              label: 'Categorie',
              dataKey: 'categorie',
            },
            {
              width: 120,
              flexGrow: 1.0,
              label: 'Titre',
              dataKey: 'title',
            },
            {
              width: 250,
              flexGrow: 1.0,
              label: 'Description',
              dataKey: 'description',
            },
      )
      break;
    case 'users':
    col.push(
            {
              width: 120,
              flexGrow: 1.0,
              label: 'email',             
              dataKey: 'emails[0]',
            },
            {
              width: 120,
              flexGrow: 1.0,
              label: 'role',
              dataKey: 'roles',
            },
      )
      break; 
  }

    return (
      <Paper 
        style={{ height:fullHeight ? innerHeight : isWidthUp('md',width) ? innerHeight-191 : innerHeight-159, width: '100%' }}
        >
        <WrappedVirtualizedTable
          deferredMeasurementCache={cache}
          headerHeight={isWidthUp('md',width) ? 60 : 40}
          rowHeight={cache.rowHeight}
          overscanRowCount={10}
          selected={selected}
          rowCount={rows.length}
          rowGetter={({ index }) => rows[index]}
          onRowClick={(event) => this.handleCheckClick(event)}
          onHeaderClick={(event) => this.handleCheckClick(event)}
          columns={col}
        />
      </Paper>
    );
  }
  
}

export default (ReactVirtualizedTable);