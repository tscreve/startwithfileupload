import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { withRouter, NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';


import SwipeableViews from 'react-swipeable-views';
import Swiper from 'react-id-swiper';

import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import ArrowLeft from '@material-ui/icons/ArrowLeft';
import ArrowRight from '@material-ui/icons/ArrowRight';
import grey from '@material-ui/core/colors/grey';
import Fab from '@material-ui/core/Fab';
import ZoomTransition from '@material-ui/core/Fade';

const styles = theme => ({
  hideOnSmall:{
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
    // [theme.breakpoints.up('md')]: {
    //  display: 'block',
    // },
    // [theme.breakpoints.up('lg')]: {
    //  display: 'block',
    // },
  },
  fabButton:{
    opacity:0.9,
    position:'fixed',
    zIndex:2000,
    top:40,
  },
  prevButton:{
    left: '30%',
    top:40,
    [theme.breakpoints.down('sm')]: {
      left: '20%',
    },
    [theme.breakpoints.down('xs')]: {
      left: 30,
    },
  },
  nextButton:{
    right: '30%',
    top:40,
    [theme.breakpoints.down('sm')]: {
      right: '20%',
    },
    [theme.breakpoints.down('xs')]: {
      right: 30,
    },
  },
  thumbnailImages:{
    margin:'auto',
    display:'block',
  },
  imagesContainer:{
    width:'120px!important',
    height:'100px!important',
    opacity:0.5,
  },
  galleryImages:{   
   margin:'auto',
  },
  zoomContainer:{
    margin:'auto',
  },
  zoomImages:{  
   margin:'auto',
   width:'100%',
  },
});


class Zoom extends Component {
  constructor(props) {
    super(props); 

    this.state = {
      gallerySwiper: null,
      thumbnailSwiper: null,
      item: this.props.item,
      count: this.props.item.length,
      id:this.props.id,
      zoom:false,
      maximise:false,
    };
    // this.galleryRef = this.galleryRef.bind(this);
    // this.thumbRef = this.thumbRef.bind(this);
  }

  componentWillUpdate(nextProps, nextState) {
    // if (nextState.gallerySwiper && nextState.thumbnailSwiper ) {
    //   const { gallerySwiper, thumbnailSwiper } = nextState

    //   gallerySwiper.controller.control = thumbnailSwiper;
    //   thumbnailSwiper.controller.control = gallerySwiper;
    // }
  }

  // galleryRef(ref) {
  //   if (ref) this.setState({ gallerySwiper: ref.swiper})
  // }

  // thumbRef(ref) {
  //   if (ref) this.setState({ thumbnailSwiper: ref.swiper })
  // }

  // slideNext(){
  //   this.state.gallerySwiper.slideNext()    
  // } 
  //  change(){
  //   if (this.state.gallerySwiper){
  //     this.props.returnId(this.state.gallerySwiper.activeIndex)
  //   }
  // }
  componentDidUpdate(){
  }

  componentDidMount(){
  }

  prevItem(e){
    const {id,count}=this.state;
    let newId=id-1;

    if(newId < 0){
      newId=count-1
    }
    this.setState({id:newId,maximise:false})  
  }

  nextItem(e){
    const {id,count}=this.state;
    let newId=id+1;

    if(newId >= count){
      newId=0
    }
    this.setState({id:newId,maximise:false})
  }

  maximise(e){
    // console.log(this.state.maximise)
    this.setState({ maximise:!this.state.maximise})  
  }

  render() {
    // const gallerySwiperParams = {  
    //   // loop:true,
    //   slidesPerView: 'auto',
    //   initialSlide:this.props.id,
    //   on: {
    //     slideChange:() => this.change(),
    //   },
    //   spaceBetween: 10,
    //   observer: true,
    //   observeParents: true,
    //   pagination: {
    //     el: '.swiper-pagination',
    //     clickable: true,
    //     renderBullet: (index, className) => {
    //       return '<span class="' + className + '">' + (index + 1) + '</span>';
    //     },
    //   },
    // };
    // const thumbnailSwiperParams = {
    //   observer: true,
    //   observeParents: true,
    //   direction: 'vertical',
    //   spaceBetween: 20,
    //   centeredSlides: true,
    //   slidesPerView: 5,
    //   touchRatio: 0.8,
    //   slideToClickedSlide: true,
    // }

    const { classes }  = this.props;
    const { item,id,maximise }  = this.state;
    let link=null;
    console.log(item)
    return (   
    <div className={classes.root} >
      <div>
        <div>
          {item ?              
            <div> 
            <Grid container >             
              {
                <Grid item xs={1} sm={1} md={1} className={classes.hideOnSmall}>
                 
               </Grid>
              } 
              <div className={classes.fabButton}>
                <Fab  
                    size="small"
                    className={`${classes.prevButton} ${classes.fabButton}`} 
                    onClick={(e) => this.prevItem(e)} >
                  <ChevronLeft />
                </Fab>
                <Fab  
                    size="small"    
                    style={{left:(innerWidth-40)/2}}
                    className={classes.fabButton} 
                    onClick={(e) => this.props.handleClose(e)} >
                  <CloseIcon />
                </Fab>
                <Fab                    
                    size="small"
                    className={`${classes.nextButton} ${classes.fabButton}`} 
                    onClick={(e) => this.nextItem(e)} >
                  <ChevronRight />
                </Fab>
               </div>
                <Grid item xs={12} sm={12} md={12} >   


                 {item.map((l,key) => (
                  <ZoomTransition 
                    in={id === key ? true : false} 
                    key={key} 
                    style={{transitionDelay:id === key ? '200ms' : '0ms'}} 
                    mountOnEnter 
                    unmountOnExit>
                    <div 
                      key={key}
                      style={{width:(!maximise && innerWidth < l[3].width) || (maximise && innerWidth > l[3].width) ? '100%' 
                                    : (maximise && innerWidth < l[3].width) || (!maximise && innerWidth > l[3].width) ? l[3].width : '100%',
                              display:id === key ? 'block' : 'none'}} 
                      className={classes.zoomContainer} >
                     <img     
                        onClick={(e) => this.maximise(e)}
                        src={l[2]}
                        className={classes.zoomImages}
                      /> 
                    </div>
                  </ZoomTransition>
                  ))

                }        
                </Grid>

                {
                 // <div  id="zoomGallery" >
                 //   <Swiper 
                 //     {...gallerySwiperParams} ref={this.galleryRef}>
                 //     {item.map((l,key) => (
                 //       <div 
                 //         key={l[2]}                     
                 //         >
                 //           <img  
                 //             onClick={(e) => this.slideNext(e)}
                 //             className={classes.galleryImages}     
                 //             src={l[2]}
                 //           />
                 //       </div>
                 //     ))}
                 //   </Swiper>  
                 // </div> 


                  // <Grid item xs={2} sm={2} md={2} >
                  // <div >              
                  //    <Swiper  {...thumbnailSwiperParams}  ref={this.thumbRef} >
                  //       {item.map((l,key) => (
                  //         <div 
                  //           className={classes.imagesContainer} 
                  //           key={l[1]} >
                  //             <img 
                  //               className={classes.thumbnailImages} 
                  //               src={l[1]}
                  //             />
                  //         </div>
                  //       ))}
                  //     </Swiper>
                  // </div>  
                  // </Grid>
                }
              {
              <Grid item xs={1} sm={1} md={1}  className={classes.hideOnSmall}>
                
              </Grid>
              }
              </Grid>
            </div>          
           :''}

        </div>


        </div>
      </div>
    );
  } 
}

Zoom.propTypes = {
  // returnId: PropTypes.func.isRequired,
  handleClose: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
};

export default withRouter(withStyles(styles)(Zoom));