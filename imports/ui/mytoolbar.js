import React, {createRef, Component } from 'react';
import { withRouter,Link,NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import classnames from 'classnames';



import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import Button from '@material-ui/core/Button';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import Badge from '@material-ui/core/Badge';
import { fade } from '@material-ui/core/styles/colorManipulator';
import MenuList from '@material-ui/core/MenuList';
import Paper from '@material-ui/core/Paper';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import ListItem from '@material-ui/core/ListItem';
import ListSubheader from '@material-ui/core/ListSubheader';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import SearchIcon from '@material-ui/icons/Search';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MoreIcon from '@material-ui/icons/MoreVert';
import MailIcon from '@material-ui/icons/Mail';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import DraftsIcon from '@material-ui/icons/Drafts';
import SendIcon from '@material-ui/icons/Send';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import StarBorder from '@material-ui/icons/StarBorder';



const isLoggedIn = () => {
  return Meteor.userId() !== null; 
};
const drawerWidth = 240;

const styles = theme => ({
  root: {
    width: '100%',
    flexGrow: 1,
  },
  grow: {
    flexGrow: 1,
  },
  hideOnSmall:{
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
    // [theme.breakpoints.up('md')]: {
    //  display: 'block',
    // },
    // [theme.breakpoints.up('lg')]: {
    //  display: 'block',
    // },
  },
  menuButton: {
    zIndex:100,
    marginLeft: -12,
    marginRight: 20,
    display: 'none',
    [theme.breakpoints.down('sm')]: {
      display: 'inline-block',
    },
  },
  topMenu: {
    display: 'none',
    position: 'relative',
    marginRight: theme.spacing.unit * 2,
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('md')]: {
      marginLeft: theme.spacing.unit * 2,
      width: 'auto',
      display: 'block',
    },    
  },
  nested: {
    paddingLeft: theme.spacing.unit * 4,
  },
  landscapePadding:{
    paddingTop:64,
    [theme.breakpoints.down('xs')]: {
      paddingTop:48,
    },   
  },
  portraitPadding:{
    paddingTop:64,
    [theme.breakpoints.down('xs')]: {
      paddingTop:56,
    },   
  },
  subMenuItem:{
    marginLeft:25,
  },
  tab : {
    minWidth:0,
    [theme.breakpoints.up('lg')]: {
      minWidth:160,
    },
  },
  imgContainer:{
    [theme.breakpoints.down('sm')]: {
      // position:'absolute',
      width:'100%',
      textAlign:'center',
      paddingRight:56,
    },
    [theme.breakpoints.down('xs')]: {
      paddingRight:48,
    },
  },
  imgLogo:{
    width:'125px',
  },
});

class MyToolbar extends React.Component {
  constructor(props) {
    super(props);

  // console.log(this.props.itemSelected)
  }
  state = {
    drawer:false,
    auth: isLoggedIn() ? true : false,
    anchorEl: null,
    tabsItem:this.props.itemSelected !== undefined ? this.props.itemSelected : false,   
    menuItem:this.props.itemSelected !== undefined ? this.props.itemSelected : false,   
  };  

  componentDidMount(){
    if ('scrollRestoration' in history) {
      // Back off, browser, I got this...
      history.scrollRestoration = 'manual';
    }
  }

  componentDidUpdate(prevProps){
    // console.log(prevProps.itemSelected);
    // console.log(this.props.itemSelected);

    if(prevProps.itemSelected !== this.props.itemSelected){
      this.setState({ 
        tabsItem:this.props.itemSelected !== undefined ? this.props.itemSelected : false,   
        menuItem:this.props.itemSelected !== undefined ? this.props.itemSelected : false,  
      });
    }
  }

  handleClose = () => {
    this.setState({ anchorEl: null });
  };
  handleProfileMenuOpen = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleMenuClose = () => {
    this.setState({ anchorEl: null });
    // this.handleMobileMenuClose();
  };


  DrawerOpen = () => {
    this.setState({ drawer: true });
  };

  DrawerClose = () => {    
    this.setState({ drawer: false });
  };

  TabsClick = (id) => {
    this.setState({ 
      tabsItem:id , 
      menuItem:id , 
    });
  };

  MenuClick = (id) => {  
    this.setState({ 
      drawer: false , 
      menuItem:id,
      tabsItem:id < 5 ? id : false,
      anchorEl: null  
    });
  };



  logout = (e) => { 
    e.preventDefault(); 
    const { history } = this.props;   
    Meteor.logout( (err) => { 
        if (err) { 
            console.log( err.reason ); 
        } 
        this.setState({ 
            anchorEl: null,
            auth:false,
            drawer: false
        });

        history.push('/home');  
    });
  }  


  //////////////////   MENU EXPENDABLE    //////////////////
  //  SubMenu = (e) => {
  //   e.preventDefault();
  //   this.setState({ submenu: !this.state.submenu });
  // };

  // handleChange = (event, tab) => {
  //   console.log(tab);
  //   this.setState({ tabsItem:tab });
  // };

  // handleMobileMenuOpen = event => {
  //   this.setState({ mobileMoreAnchorEl: event.currentTarget });
  // };

  // handleMobileMenuClose = () => {
  //   this.setState({ mobileMoreAnchorEl: null });
  // };
  // handleMenu = event => {
  //   this.setState({ anchorEl: event.currentTarget });
  // };
  //////////////////////////////////////////////////////////

  render() {
    const { classes,theme,location,match,width } = this.props;
    const { auth, anchorEl,mobileMoreAnchorEl,drawer,tabsItem } = this.state;
    const isMenuOpen = Boolean(anchorEl);
    const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);
    const isAdmin=Roles.userIsInRole(Meteor.user(),['admin'])

    let paddingClassName,barPos;

    // console.log(isAdmin)

    if(match.path === '/favorite' || match.path === '/mes_annonces' || match.path === '/discussions'){
      barPos="fixed"
      paddingClassName= innerWidth > innerHeight ? classes.landscapePadding : classes.portraitPadding;
    }else{
      barPos="static"
    }

    // const divClassName =  (location.pathname === '/home' || location.pathname === '/') ? classes.root : (classes.root,paddingClassName);
    ////////////////////    rendre la barre d outil invisible     ////////////////////////////////////
    // const appBarClassName =(location.pathname === '/home'  || location.pathname === '/') ? classes.hidebar : '';
    const appBarClassName ='';

    return (
      <div className={`${classes.root} ${paddingClassName}`}  > 
      <AppBar position={barPos} id="MyAppBar">
          <Toolbar> 
            <IconButton className={classes.menuButton} 
                        color="inherit" 
                        aria-label="Open drawer"
                        onClick={this.DrawerOpen}>
              <MenuIcon />
            </IconButton>
            <div className={classes.imgContainer} >
              <Link   
                
                onClick={() => this.TabsClick(false)}
                to="/Home" >
                  <img 
                  className={classes.imgLogo} 
                  src="/images/index.png"
                  />
              </Link>
            </div>

            <div className={classes.topMenu}>
          <Tabs 
              value={tabsItem}
              onChange={this.handleChange}>
            <Tab 
                label="DÉPOSER UNE ANNONCE" 
                to="/deposer_une_annonce" 
                className={classes.tab} 
                component={Link}  
                onClick={() => this.TabsClick(0)}/>
            <Tab 
                label="OFFRES" 
                to="/offre" 
                className={classes.tab} 
                component={Link}  
                onClick={() => this.TabsClick(1)}/>
            <Tab 
                label="DEMANDES" 
                to="/demande" 
                className={classes.tab} 
                component={Link}  
                onClick={() => this.TabsClick(2)}/>

            {isLoggedIn() &&
              <Tab 
                  label="Mes favoris" 
                  to="/favorite" 
                  className={classes.tab} 
                  component={Link}  
                  onClick={() => this.TabsClick(3)}/> }

             {isLoggedIn() &&      
              <Tab 
                  label= {<Badge  color="secondary" badgeContent={4}>
                            Mes messages
                          </Badge> }
                  to="/discussions" 
                  className={classes.tab} 
                  component={Link}  
                  onClick={() => this.TabsClick(4)}/>}
          </Tabs>

           </div>
           <div className={classes.grow} />             
           <div className={classes.hideOnSmall}>
           {
              isLoggedIn() ? 
              <div>              
                {
                  // <IconButton color="inherit" >
                  //   <Badge  badgeContent={4} color="secondary">
                  //     <MailIcon />
                  //   </Badge>
                  // </IconButton>
                  }       
                <IconButton
                  aria-owns={isMenuOpen ? 'material-appbar' : null}
                  aria-haspopup="true"
                  onClick={this.handleProfileMenuOpen}
                  color="inherit"
                >
                    <AccountCircle />
                </IconButton>
              </div>
                :
              <Button variant="contained" component={Link} to="/login">Login</Button>
            }   
            </div>    
          </Toolbar>
        </AppBar>

        
        <Menu
          anchorEl={anchorEl}
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          transformOrigin={{ vertical: 'top', horizontal: 'right' }}
          open={isMenuOpen}
          onClose={this.handleMenuClose}
        >
          <MenuItem 
              onClick={() => this.MenuClick(5)} 
              component={Link} 
              to="/mes_annonces"
          >Mes annonces</MenuItem>
          {
            isAdmin && <MenuItem 
                            onClick={() => this.MenuClick(6)} 
                            component={Link} 
                            to="/admin"
                        >Admin</MenuItem>
          }
          <MenuItem 
              onClick={this.logout}
          >Logout</MenuItem>
        </Menu>

      <SwipeableDrawer       
        anchor='left'
        open={drawer}
        onClose={this.DrawerClose}
        onOpen={this.DrawerOpen}
        classes={{
          paper: classes.drawerPaper,
        }}
      >     
        
      <div
        tabIndex={0}
        role="button"
        onClick={this.DrawerClose}
        onKeyDown={this.DrawerClose}
      >
      </div>
      <div className={classes.drawerHeader}>
        <IconButton onClick={this.DrawerClose}>
          {<ChevronLeftIcon />}
        </IconButton>
      </div>
        
          <MenuList>
            <MenuItem 
                selected={this.state.menuItem === 0} 
                className={classes.menuItem} 
                component={Link} 
                to="/deposer_une_annonce" 
                onClick={() => this.MenuClick(0)}>
              <ListItemIcon className={classes.icon}>
                <SendIcon />
              </ListItemIcon>
              <ListItemText classes={{ primary: classes.primary }} inset primary="DÉPOSER UNE ANNONCE" />
            </MenuItem>
            <MenuItem 
              selected={this.state.menuItem === 1} 
              className={classes.menuItem} 
              component={Link} 
              to="/offre" 
              onClick={() => this.MenuClick(1)}>
              <ListItemIcon className={classes.icon}>
                <SendIcon />
              </ListItemIcon>
              <ListItemText classes={{ primary: classes.primary }} inset primary="OFFRES" />
            </MenuItem>
            <MenuItem 
              selected={this.state.menuItem === 2} 
              className={classes.menuItem} 
              component={Link} 
              to="/demande" 
              onClick={() => this.MenuClick(2)}>
              <ListItemIcon className={classes.icon}>
                <SendIcon />
              </ListItemIcon>
              <ListItemText classes={{ primary: classes.primary }} inset primary="DEMANDES" />
            </MenuItem>
             <Divider />
             <ListSubheader component="div">{"Mon espace"}</ListSubheader>
             {isLoggedIn() ?
                <div>
                <MenuItem 
                  selected={this.state.menuItem === 3} 
                  className={classes.subMenuItem} 
                  component={Link} 
                  to="/favorite" 
                  onClick={() => this.MenuClick(3)}>
                  <ListItemIcon className={classes.icon}>
                    <SendIcon />
                  </ListItemIcon>
                  <ListItemText classes={{ primary: classes.primary }} inset primary="Mes favoris" />
                </MenuItem>
                <MenuItem 
                  selected={this.state.menuItem === 4} 
                  className={classes.subMenuItem} 
                  component={Link} 
                  to="/discussions" 
                  onClick={() => this.MenuClick(4)}>
                  <Badge  badgeContent={4} color="secondary">
                    <ListItemIcon className={classes.icon}>
                      <DraftsIcon />
                    </ListItemIcon>
                    <ListItemText classes={{ primary: classes.primary }} inset primary="Mes messages" />
                  </Badge>
                </MenuItem>         
                <MenuItem 
                  selected={this.state.menuItem === 5} 
                  className={classes.subMenuItem} 
                  component={Link} 
                  to="/mes_annonces" 
                  onClick={() => this.MenuClick(5)}>
                  <ListItemIcon className={classes.icon}>
                    <SendIcon />
                  </ListItemIcon>
                  <ListItemText classes={{ primary: classes.primary }} inset primary="Mes annonces" />
                </MenuItem>   

                {isAdmin &&            
                  <MenuItem 
                    selected={this.state.menuItem === 6} 
                    className={classes.subMenuItem} 
                    component={Link} 
                    to="/admin" 
                    onClick={() => this.MenuClick(6)}
                    >
                    <ListItemIcon className={classes.icon}>
                      <SendIcon />
                    </ListItemIcon>
                    <ListItemText classes={{ primary: classes.primary }} inset primary="Admin" />
                  </MenuItem> }


                <Divider />
                <MenuItem 
                  // className={classes.subMenuItem} 
                  onClick={this.logout}>
                  <ListItemIcon className={classes.icon}>
                    <SendIcon />
                  </ListItemIcon>
                  <ListItemText classes={{ primary: classes.primary }} inset primary="Logout" />
                </MenuItem> 
                </div>
                :
                <MenuItem 
                  component={Link} 
                  to="/login" 
                  className={classes.subMenuItem} 
                  onClick={() => this.MenuClick(false)}>
                  <ListItemIcon className={classes.icon}>
                    <SendIcon />
                  </ListItemIcon>
                  <ListItemText classes={{ primary: classes.primary }} inset primary="Login" />
                </MenuItem> 

              }   
            {//////   MENU EXPENDABLE    //////
            
              // <MenuItem onClick={this.SubMenu}>
              // <ListItemIcon className={classes.icon} >
              //   <InboxIcon />
              // </ListItemIcon>
              // <ListItemText classes={{ primary: classes.primary }} inset primary="Mon compte"/>                
              //     {this.state.submenu ? <ExpandLess /> : <ExpandMore />}            
              // </MenuItem>
              //  <Collapse in={this.state.submenu} timeout="auto" unmountOnExit>            
              //     <List component="div" disablePadding>
              //       <ListItem component={Link} to="/home" className={classes.nested} onClick={this.DrawerClose}>
              //         <ListItemIcon className={classes.icon}>               

              //         <StarBorder />
                       
              //         </ListItemIcon>
              //         <ListItemText classes={{ primary: classes.primary }} inset primary="Profile" />
              //       </ListItem>
              //     </List>                
              //   </Collapse>
              }
            </MenuList>
       
      </SwipeableDrawer>
      </div>
    );
  }
}

MyToolbar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withRouter(withStyles(styles)(MyToolbar));
