import React from 'react';

import { withRouter,Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import Avatar from '@material-ui/core/Avatar';
import FolderIcon from '@material-ui/icons/Folder';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

import MoreVertIcon from '@material-ui/icons/MoreVert';
import DeleteIcon from '@material-ui/icons/Delete';
import Description from '@material-ui/icons/Description';

const styles = theme => ({
    menuItem: {
    '&:focus': {
      backgroundColor: theme.palette.primary.main,
      '& $primary, & $icon': {
        color: theme.palette.common.white,
      },
    },
  },
    primary: {},
  icon: {},

  listItem:{
    // paddingtop:0,
    // paddingbottom:0,
  },
});

class IndividualFile extends React.Component {
    constructor(props) {
    super(props);

 
    // console.log(this.props);
  }

 state = {   
    anchorEl: null,
  }; 


  removeFile = () => {
    "use strict";
    console.log(this.props);

    let conf = confirm('Are you sure you want to delete the file?') || false;
    if (conf == true) {
      this.handleClose();
      Meteor.call('RemoveFile', this.props.fileId, function (err, res) {
        if (err)
          console.log(err);
      });
    }
  };

  renameFile = () => {
    "use strict";

    let validName = /[^a-zA-Z0-9 \.:\+()\-_%!&]/gi;
    let prompt    = window.prompt('New file name?', this.props.fileName);

    // Replace any non valid characters, also do this on the server
    if (prompt) {
      prompt = prompt.replace(validName, '-');
      prompt.trim();
    }

    if (!_.isEmpty(prompt)) {
      Meteor.call('RenameFile', this.props.fileId, prompt, function (err, res) {
        if (err)
          console.log(err);
      });
    }
  };

  toThumbnail= () => {
    window.location.assign(this.props.thumbnail);
  };

  toPreview= () => {
    window.location.assign(this.props.preview);
  };

  toPreview2= () => {
    window.location.assign(this.props.preview2);
  };

  toFile = () => {
    window.location.assign(this.props.fileUrl);
  };

  handleMenu = event => {
      event.preventDefault(); 
  this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  render() {
    const { classes } = this.props;    
    const {  anchorEl } = this.state;  
    const open = Boolean(anchorEl);  
    // console.log(this.props);

    return ( 
            <ListItem                           
                button 
                onClick={this.toFile}                
                >                
              {
                this.props.preview !== null ?
                 <Avatar alt="" src={this.props.thumbnail}/>
                 :
                 <Avatar>
                   {this.props.isPDF  && <Description/>}
                   {this.props.isText  && <FolderIcon/>}                  
                </Avatar>
              } 
             
              <ListItemText primary={this.props.fileName} />
              
              <ListItemSecondaryAction>
                 <IconButton
                  aria-owns={open ? 'menu' : null}
                  aria-haspopup="true"
                  onClick={this.handleMenu}
                  color="inherit"
                >
                <MoreVertIcon/>
                </IconButton>
                <Menu
                  id="menu"
                  anchorEl={anchorEl}
                  anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  open={open}
                  onClose={this.handleClose}
                >
                  <MenuItem onClick={this.removeFile}><DeleteIcon/></MenuItem>
                  <MenuItem onClick={this.toThumbnail}>To thumbnail</MenuItem>
                  <MenuItem onClick={this.toPreview}>To preview</MenuItem>
                  <MenuItem onClick={this.toPreview2}>To preview 2</MenuItem>                 
                </Menu>
              </ListItemSecondaryAction>
            </ListItem>
            
          
          //       return <div className="m-t-sm">
          //   <div className="row">
          //     <div className="col-md-12">
          //       <strong>{this.props.fileName}</strong>
          //       <div className="m-b-sm">
          //       </div>
          //     </div>
          //   </div>

          //   <div className="row">
          //     <div className="col-md-3">
          //       <button onClick={this.renameFile} className="btn btn-outline btn-primary btn-sm">
          //         Rename
          //       </button>
          //     </div>


          //     <div className="col-md-3">
          //       <a href={this.props.fileUrl} className="btn btn-outline btn-primary btn-sm"
          //          target="_blank">View</a>
          //     </div>

          //     <div className="col-md-2">
          //       <button onClick={this.removeFile} className="btn btn-outline btn-danger btn-sm">
          //         Delete
          //       </button>
          //     </div>

          //     <div className="col-md-4">
          //       Size: {this.props.fileSize}
          //     </div>
          //   </div>
          // </div>

          
    );


  }
};

IndividualFile.propTypes= {
  fileName: PropTypes.string.isRequired,
  fileSize: PropTypes.number.isRequired,
  fileUrl: PropTypes.string,
  fileId: PropTypes.string.isRequired,
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(IndividualFile);