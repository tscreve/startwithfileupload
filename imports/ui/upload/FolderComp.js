
import { Meteor } from 'meteor/meteor';
import React from 'react';
import { withRouter,Link } from 'react-router-dom';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import FolderIcon from '@material-ui/icons/Folder';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

import MoreVertIcon from '@material-ui/icons/MoreVert';
import DeleteIcon from '@material-ui/icons/Delete';



const styles = theme => ({
    menuItem: {
    '&:focus': {
      backgroundColor: theme.palette.primary.main,
      '& $primary, & $icon': {
        color: theme.palette.common.white,
      },
    },
  },
    primary: {},
  icon: {},

  listItem:{
    // paddingtop:0,
    // paddingbottom:0,
  },
});

class FolderComp extends React.Component {
    constructor(props) {
    super(props);
  

 this.state = {   
    anchorEl: null,
  }; 
}

  handleMenu = event => {
      event.preventDefault(); 
  this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  removeFolder = (e) => {
     e.preventDefault();
    Meteor.call('userFolders.remove',this.props.folderId);
    this.setState({ anchorEl: null });
  };

  render() {
     const { classes } = this.props;    
     const {  anchorEl } = this.state;  
     const open = Boolean(anchorEl);  
     const link=this.props.path.concat(this.props.name).concat(".");
     // console.log('link',link);

    return ( 
             <ListItem  
                component={Link} 
                to={link}                               
                button 
                            
                > 
                 <Avatar>                   
                 <FolderIcon/>               
                </Avatar>
              <ListItemText primary={this.props.name} />             
              
              <ListItemSecondaryAction>
                 <IconButton
                  aria-owns={open ? 'menu' : null}
                  aria-haspopup="true"
                  onClick={this.handleMenu}
                  color="inherit"
                >
                <MoreVertIcon/>
                </IconButton>
                <Menu
                  id="menu"
                  anchorEl={anchorEl}
                  anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  open={open}
                  onClose={this.handleClose}
                >
                  <MenuItem onClick={this.handleClose}>Profile</MenuItem>
                  <MenuItem onClick={this.handleClose}>My account</MenuItem>
                  <MenuItem onClick={this.removeFolder}><DeleteIcon/></MenuItem>
                </Menu>
              </ListItemSecondaryAction>
            </ListItem>
    );
  }
};

export default withStyles(styles)(FolderComp);