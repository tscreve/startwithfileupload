import { ReactMeteorData } from 'meteor/react-meteor-data';
import React from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import { withRouter, Link } from 'react-router-dom';
// import { _ } from 'meteor/underscore';
import { _app, Collections } from '../../lib/core.js';

import { withStyles } from '@material-ui/core/styles';
import IndividualFile from './FileIndividualFile.js';
import FolderComp from './FolderComp.js';
import { Folders } from '../../api/userFolder.js';


import List from '@material-ui/core/List';
import AddIcon from '@material-ui/icons/Add';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Input from '@material-ui/core/Input';
import CircularProgress from '@material-ui/core/CircularProgress';

var createReactClass = require('create-react-class');

const styles = theme => ({
   progress: {
    margin: 40,
  },
});

var UploadFormComponent = createReactClass({
  mixins: [ReactMeteorData],

getMeteorData() {   
  var handle = Meteor.subscribe('files.all') && Meteor.subscribe('userFolders');
  return {

    docsReadyYet: handle.ready(),
    docs: Collections.files.find().fetch(), // Collection is UserFiles
    folders:Folders.find({path:this.state.userPath}).fetch()
  };  
},

getInitialState(){
  return {
    userPath:':',
    newFolderName:'',
    uploading: [],
    progress: 0,
    inProgress: false,
    loader:false
  }
},

componentDidUpdate() {
  const { match } = this.props;    
  const userPath = (match && match.params && match.params.userpath) || ':'; 
  // console.log('update',userPath);
  console.log(this.state);
  // console.log(this.props);

  if(this.state.userPath !== userPath){      
    this.setState({userPath:userPath});
  }
},

uploadIt(e){
  "use strict";
  e.preventDefault();

  const { history } = this.props; 
  let self = this;

  if (e.currentTarget.files && e.currentTarget.files[0]) {
    // We upload only one file, in case
    // there was multiple files selected
    var file = e.currentTarget.files[0];
    console.log(file);

    if (file) {
      let uploadInstance = Collections.files.insert({
        file: file,
         
        meta: { 
          userPath:self.state.userPath,          
          locator: self.props.fileLocator,
          userId: Meteor.userId() // Optional, used to check on server for file tampering
        },
        streams: 'dynamic',
        chunkSize: 'dynamic',
        allowWebWorkers: true // If you see issues with uploads, change this to false
      }, false);

      self.setState({
        uploading: uploadInstance, // Keep track of this instance to use below
        inProgress: true, // Show the progress bar now
        loader:true
      });

      // These are the event functions, don't need most of them, it shows where we are in the process
      uploadInstance.on('start', function () {
        console.log('Starting');
        console.log(uploadInstance);
      });

      uploadInstance.on('end', function (error, fileObj) {
        console.log('On end File Object: ', fileObj);
      });

      uploadInstance.on('uploaded', function (error, fileObj) {
        console.log('uploaded: ', fileObj);
        

        // Remove the filename from the upload box
        // self.refs['fileinput'].value = '';

        // Reset our state for the next file
        // self.setState({
        //   uploading: [],
        //   progress: 0,
        //   inProgress: false
        // });
      });

      uploadInstance.on('error', function (error, fileObj) {
        console.log('Error during upload: ' + error);
      });

      uploadInstance.on('progress', function (progress, fileObj) {
        console.log('Upload Percentage: ' + progress);
        if(progress === 100){
          // self.refs['fileinput'].value = '';
          // Reset our state for the next file
          self.setState({
            uploading: [],
            progress: 0,
            inProgress: false
          });
        }else{
          // Update our progress bar
          self.setState({
            progress: progress
          })          
        }
      });
      uploadInstance.start(); // Must manually start the upload
      // history.goBack();
      Meteor.setTimeout(() => {history.goBack();}, 0);      
    }
  }
},

createFolder(e){
  e.preventDefault();

  const { history } = this.props; 
  let name=this.state.newFolderName;
  let path=this.state.userPath;
 
 
   Meteor.call('userFolders.insert', 
      path,
      name
      , (err, res) => {
        if (err) {
          alert(err);
          history.goBack();
        } else {
          console.log(res);
          if(res==false){
            alert('existe deja');
          }else{
             history.goBack();
          }          
        // success!
      }
    });
  },

handlchange(e){
   e.preventDefault();
   this.setState({newFolderName:e.currentTarget.value});
},

goBack(e){
    e.preventDefault();
    const { history } = this.props; 

    history.goBack();
},

  render() {
    const { classes } = this.props;
    const loader=this.state.loader;
    // loader=true;    
   
      'use strict';
      return <div>
          <Dialog
            open={true}
            onClose={((e) => this.goBack(e))}
            aria-labelledby="form-dialog-title">
          { !loader ?
          <DialogTitle id="form-dialog-title">New folder</DialogTitle>
          : ''
          }

          <DialogContent>
          { !loader ?
            <div>
              <DialogContentText>
              Enter a name.
            </DialogContentText>
            <form className="new-task" >
              <div>
                <Input  type="file" 
                      id="fileinput" 
                      disabled={this.state.inProgress} 
                      ref="fileinput"
                    
                    onChange={((e) => this.uploadIt(e))}/>        
              </div>
              <div>
              <Input                
                id="folderName"  
                margin="dense"              
                label="Name"
                type="text"
                onChange={((e) => this.handlchange(e))}
              />
             </div>
            </form> 
            </div>    
            : 
            <CircularProgress className={classes.progress} size={50} /> 
          }

          </DialogContent>
          { !loader ?
            <div>
              <DialogActions>
              <Button onClick={((e) => this.goBack(e))} color="primary">
                Cancel
              </Button>
              <Button onClick={this.createFolder} color="primary">
                Create folder
              </Button>
            </DialogActions>
            </div>
          : ''
          }
        </Dialog>
      </div> 
     
  }
});

export default withRouter(withStyles(styles)(UploadFormComponent));
