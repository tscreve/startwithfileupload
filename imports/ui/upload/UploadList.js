import { ReactMeteorData } from 'meteor/react-meteor-data';
import React from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import { withRouter, Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
// import { _ } from 'meteor/underscore';
import { _app, Collections } from '../../lib/core.js';

import IndividualFile from './FileIndividualFile.js';
import FolderComp from './FolderComp.js';
import { Folders } from '../../api/userFolder.js';


import List from '@material-ui/core/List';
import AddIcon from '@material-ui/icons/Add';
import Button from '@material-ui/core/Button';
import Fab from '@material-ui/core/Fab';



var createReactClass = require('create-react-class');

const styles = theme => ({
  root: {    
    paddingTop:0,
    paddingBottom:0,
    // width: '100%',
    // maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  fab: {
    position: 'fixed',
    bottom: theme.spacing.unit * 2,
    right: theme.spacing.unit * 2,
  },
});

var UploadListComponent = createReactClass({
  mixins: [ReactMeteorData],
  getMeteorData() {   
  var handle = Meteor.subscribe('files.all') && Meteor.subscribe('userFolders');
  return {

    docsReadyYet: handle.ready(),
    docs: Collections.files.find().fetch(), // Collection is UserFiles
    folders:Folders.find({path:this.state.userPath}).fetch()

  };
  
},

  getInitialState(){
    return {
      userPath:'',
    }
  },

componentDidUpdate() {
  const { match } = this.props;    
  const userPath = (match && match.params && match.params.userpath) || ''; 
  // console.log('update',userPath);
  // console.log(this.state);
  // console.log(this.data);
  // console.log(this.props);

  if(this.state.userPath !== userPath){      
    this.setState({userPath:userPath});
    console.log('userPath');
  }
},

  render() {
    console.log(this.data.docs);
    const { classes } = this.props;     

    if (this.data.docsReadyYet) {
      'use strict';
      let fileCursors = this.data.docs;
      let userFolders=this.data.folders;
      let pathLink='/upload/'.concat(this.state.userPath);
      // console.log('fileCursors',fileCursors);
      // console.log('Collections.files',Collections.files);
      // Run through each file that the user has stored
      // (make sure the subscription only sends files owned by this user)
      let display = fileCursors.map((aFile, key) => {
        if (aFile.meta.userPath === this.state.userPath){
               
        let link = Collections.files.findOne({_id: aFile._id}).link();  //The "view/download" link
        let link1 = Collections.files.findOne({_id: aFile._id}).link('preview');
        let link2 = aFile.versions.thumbnail40 ? Collections.files.findOne({_id: aFile._id}).link('thumbnail40') : null;
        let link3 = Collections.files.findOne({_id: aFile._id}).link('preview2');
        console.log(aFile); 
        // Send out components that show details of each file
          return <div  key={'file' + key} >       
          <IndividualFile
            isPDF={aFile.isPDF}
            isText={aFile.isText}
            fileName={aFile.name}
            fileUrl={link}
            fileId={aFile._id}
            fileSize={aFile.size}
            preview={aFile.isImage ? link1 : null}
            thumbnail={link2}
            preview2={link3}
          />          
        </div> 
        }        
      });

      let folders = userFolders.map((afolder, key) => {
        return <div  key={'folder' + key}   >
                <FolderComp 
                  folderId={afolder._id}
                  name={afolder.name}
                  path={afolder.path}                 
                />
               </div>         
      });
      return <div>      
         <List className={classes.root}>
         {folders}
        {display}
        </List>
            <Fab                
                component={Link} 
                to={pathLink}          
                className={classes.fab}
                color="primary">
              <AddIcon/>
            </Fab>
      </div>
    }
    else return <div></div>
  }
});

export default withRouter(withStyles(styles)(UploadListComponent));
