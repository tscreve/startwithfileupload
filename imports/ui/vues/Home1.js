import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { withRouter, Link } from 'react-router-dom';
import compose from 'recompose/compose';
import { Session } from 'meteor/session'


import StartMap from '../comps/startMap.js';
import MyCat from '../comps/DivSelectCatComp.js'
import MyRegion from '../../api/region.js'

import withWidth,{ isWidthUp } from '@material-ui/core/withWidth';
import { withStyles } from '@material-ui/core/styles';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox'; 
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';


const styles = theme => ({
  root: {
    flexGrow: 1,
    maxWidth:'97.5%',
    marginLeft:'1.5%',
  },
  hideOnSmall:{
    [theme.breakpoints.down('xs')]: {
      display: 'none',
    },
  },
  hideOnMedium:{
    [theme.breakpoints.down('md')]: {
      display: 'none',
    },
  },
  button:{
    width:'90%',
    marginTop:150,
    [theme.breakpoints.down('sm')]: {
      marginTop: 100,
    },
    [theme.breakpoints.down('xs')]: {
      marginTop:30,
      width:'80%',
      marginLeft:'10%',
    },
  },
  bottomButton:{
    marginTop:30,
    marginBottom:20,   
  },
  list:{
    paddingTop:40,
    paddingLeft:20,
  }
});

class Home1 extends Component {
  constructor(props) {
    super(props);
 
    this.getSize=this.getSize.bind(this)
    
    this.state = {
      scale:"",
    };
    Session.set('search',null);
    // console.log(this.props);
  }

getSize() {
  console.log(innerWidth);  
  const width=this.props.width;
  // console.log(width);
 if(width === 'md'){
    let scale='scale(0.9),translate(' + (innerWidth/2-530)/2 + ',0)';
    this.setState({
      scale:scale,
    });
  }else if(width === 'lg'){
    let scale='scale(0.9),translate(' + ((innerWidth*5/12)-530)/2 + ',0)';
    this.setState({
      scale:scale,
    });
  }else if(width === 'xl'){
    let scale='scale(0.9),translate(' + ((innerWidth/3)-530)/2 + ',0)';
    this.setState({
      scale:scale,
    });
  }
}

hideDiv(e){ 
   // e.preventDefault(); 
} 

onChange(e){ 
   // e.preventDefault(); 
} 


componentDidMount() {
  this.getSize();
  window.addEventListener("resize", this.getSize,false);
}

 componentWillUnmount() {
    window.removeEventListener("resize", this.getSize,false);
    // console.log('umount');
  }

 renderRegion(c) {    
    let i,region=[],o;
    let obj=JSON.parse(MyRegion);
    // console.log(MyRegion);

      for(o in obj){
        region.push([obj[o]['id'],obj[o]['text']]);
        for(i=0;i<=obj[o]['dept'].length-1;i++){
            // region.push(obj[o]['dept'][i]['text']);
            // console.log(obj[o]['dept'][i]['text']);
        }
      }  
      // console.log(region);  
      if(c === 'list'){
        return region.map((r,key) => {
          return (
            <li key={key}>
              <Link to={"offres/region/" + r[0] + ""}>
                {r[1]}
              </Link> 
            </li> 
          );       
        });      
      }else{
        return region.map((r,key) => {
          return (
            <MenuItem 
              component={Link} 
              to={"offres/region/" + r[0] + ""}              
              key={key}
              value={r[0]}>
              {r[1]}
            </MenuItem>
          );       
        });       
      }     

  }
 
  render() {
    const { classes,width } = this.props;
    // console.log(isWidthUp('sm',width))
    // console.log(width)
    // console.log(innerWidth)
    return (
      <div className={classes.root}>
      <Grid container >

          <Grid item lg={1} xl={2} className={classes.hideOnMedium}>
           
          </Grid>

        <Grid item xs={12} sm={6} md={3} lg={3} xl={2}>
          <Button 
              component={Link} 
              to="/deposer_une_annonce" 
              variant="contained" 
              color="primary" 
              className={classes.button}>
            Deposer une annonce
          </Button>
          <Button variant="outlined" color="primary" className={`${classes.button} ${classes.bottomButton}`}>
            chercher autour de moi
          </Button>
        </Grid>     
          


        <Grid item xs={12} sm={6} md={6} lg={5} xl={4}>

        {
        // (isWidthUp('sm',width)) && !(innerWidth > 960 && innerWidth < 1060) ?
        (isWidthUp('md',width)) ?
         <StartMap                   
              width={530}  
              height={620} 
              transform={this.state.scale}
              />
          :
         <StartMap  
              viewBox="0 0 530 620"
              /> 
        }
        </Grid>

          <Grid item xs={12} sm={12} md={3} lg={2} xl={2}>
          {isWidthUp('md',width) ?
              <ul className={classes.list}>
                {this.renderRegion('list')}
              </ul>        
            :
          <TextField
            style={{width:'100%',margin:'30px 0 30px 0'}}
            variant="outlined"
            value=""
            label="Choisissez une region" 
            select
          >
            {this.renderRegion('select')} 
          </TextField>      
          }
          </Grid>




        <Grid item lg={1} xl={2} className={classes.hideOnMedium}>
         
        </Grid>

        <Grid item  lg={1} xl={2} className={classes.hideOnMedium}>
         
        </Grid>

        <Grid item sm={12} md={12} lg={10} xl={8} className={classes.hideOnSmall}>
          <h1>Categories</h1>
          <MyCat 
            context="home"
            hideDiv={(e) => this.hideDiv(e)} 
            onChange={(e) => this.onChange(e)}            
            />          
        </Grid>
      
          <Grid item lg={1} xl={2} className={classes.hideOnMedium}>
           
          </Grid>

      </Grid> 
      </div>
    );
  } 
}

Home1.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default compose(
  withStyles(styles),
  withWidth(),
)(Home1);