import { Meteor } from 'meteor/meteor';
import { ReactMeteorData } from 'meteor/react-meteor-data';
import React, { createRef,Component } from 'react';
import ReactDOM from 'react-dom';
import { withRouter } from 'react-router-dom';
import { withTracker } from 'meteor/react-meteor-data'; 
import { _app, Collections } from '../../lib/core.js';
import compose from 'recompose/compose';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { browserHistory } from 'react-router';

import MyCat from '../../api/categorie.js';
import MyRegion from '../../api/region.js';
import { Annonces } from '../../api/annonces.js';
import LineGridList from'../comps/lineGridList.js';
import AutoComplete from'../comps/autocomplete.js';
import LocateMap from '../comps/map.js';

import withWidth,{ isWidthUp } from '@material-ui/core/withWidth';
import { withStyles } from '@material-ui/core/styles';
import { fade } from '@material-ui/core/styles/colorManipulator';
import MenuItem from '@material-ui/core/MenuItem';
import  Input from '@material-ui/core/Input';
import InputBase from '@material-ui/core/InputBase';
import CoreTextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
import CircularProgress from '@material-ui/core/CircularProgress';
import Badge from '@material-ui/core/Badge';
import green from '@material-ui/core/colors/green';
import InputAdornment from '@material-ui/core/InputAdornment';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Fab from '@material-ui/core/Fab';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Dialog from '@material-ui/core/Dialog';
import Slide from '@material-ui/core/Slide';
import Divider from '@material-ui/core/Divider';


import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';

import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
// import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';


import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import AddIcon from '@material-ui/icons/Add';
import PhotoCamera from '@material-ui/icons/PhotoCamera';
import LocationIcon from '@material-ui/icons/MyLocation';
import CheckIcon from '@material-ui/icons/Check';
import CloseIcon from '@material-ui/icons/Close';
import ArrowLeft from '@material-ui/icons/ArrowBack';
import SearchIcon from '@material-ui/icons/Search';

import {HiddenField,SelectField,AutoForm,AutoField,BoolField, TextField,LongTextField,NumField, RadioField, SubmitField,ErrorField, ErrorsField} from 'uniforms-material';


import resizeImage from 'smart-img-resize';

const isLoggedIn = () => {
  return Meteor.userId() !== null; 
};

const styles = theme => ({
  root: {
    flexGrow: 1,
    width:'99%',
    margin:'auto',
  },
  hideOnXsmall:{
    [theme.breakpoints.down('xs')]: {
      display: 'none',
    },
  },
  hideOnSmall:{
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
    // [theme.breakpoints.up('md')]: {
    //  display: 'block',
    // },
    // [theme.breakpoints.up('lg')]: {
    //  display: 'block',
    // },
  },
  field: {
    width: '100%',
    textAlign:'left',
    marginBottom:15,
    [theme.breakpoints.down('sm')]: {
      width: '95%',
    },
  },
  photos:{
    width:150,
    height:150,
    backgroundColor:'red',
    float:'inline-start',
  },
  wrapper:{
    position: 'absolute',
    top:170,
    left:50,
    zIndex:1,
  },
  buttonProgress: {
    position:'absolute',
    top: 8,
    left: '70%',
    zIndex: 1,
  },
  divider:{
    margin:15,
  },
  searchPlacefield: {
    marginTop: 20,
  },
  inputDivider:{
    margin:'0 10px 0 10px',
    height:40,
    width:1,
  },
  map:{
    marginTop:15,
    height:300,
  },
  search: {    
    flexGrow: 1,
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing.unit * 2,
    marginLeft: theme.spacing.unit,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing.unit * 3,
      width: 'auto',
    },
  },
  toolbar:{    
    [theme.breakpoints.down('xs')]: {
      paddingLeft:10
    },
  },
  inputRoot: { 
    color: 'inherit', 
    width: '100%', 
  }, 
  inputInput: {
    color: 'inherit',
    paddingTop: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
    paddingLeft: theme.spacing.unit,
    transition: theme.transitions.create('width'),
    width: '100%',
  },
  progress:{
    position: 'absolute',
    left:-6,
    bottom:-6,
  },
  zoomPaperReturnAddress:{
    opacity:0.9,
    zIndex:2000,
    position:'absolute',
    bottom:20,
    left:'5%',
    width:'90%',
    padding: theme.spacing.unit * 2,
  },
  fabLocation:{
    backgroundColor:'white',
    position:'absolute',
    top:70,
    right:20,
    zIndex:2000,
    opacity:0.9,
  },
  submitButton:{
    margin:"15px 0 10px 0",
  },
  blueLabel:{
    // color:'blue'
  }
});

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

class DepotAnnonce extends Component {
  constructor(props) {
    super(props);

    const { match } = this.props;
    const id = (match && match.params && match.params.id) || null;
    // console.log(id); 
    let i,cat=[],o;
    let obj=JSON.parse(MyCat);
    // console.log(obj);

    for(o in obj){
      // console.log(obj[o]['id']);
      cat.push([obj[o]['id'],"-- " + obj[o]['text'] + " --"]);
      // console.log(obj[o]['text']);
      for(i=0;i<=obj[o]['sub'].length-1;i++){
          cat.push([obj[o]['sub'][i]['id'],obj[o]['sub'][i]['text']]);
          // console.log(obj[o]['sub'][i]['text']);
      }
    } 

    console.log(Annonces)

    this.state = {
      id:id,
      tempValues:{},
      hideCompleted: false,
      schema:Annonces.insertSchema,
      docs: [], // Collection is UserFiles
      photos:[],
      progress:0,
      loading:false,
      success:false,
      listCat:cat,
      tempAddr:'',
      address:"",
      oldAddress:"",
      search:false,
      dialog:false,
      latlng:{},
      bAddPhoto:false,
      returnAddress:'',
      postcode:"",
      errorValidation:false,
      errorInsert:false,
      snackbar:false,
      error:"",
    };
  } 

urltoFile(url, filename, mimeType){
    return (fetch(url)
        .then(function(res){return res.arrayBuffer();})
        .then(function(buf){return new File([buf], filename, {type:mimeType});})
    );
}

uploadIt(e){
  e.preventDefault();
  "use strict";  
  let self = this;

  if (e.currentTarget.files && e.currentTarget.files[0]) {
    // We upload only one file, in case
    // there was multiple files selected
    var file = e.currentTarget.files[0];
    // console.log(file);
    if (file) {
        let fileName=file.name
        resizeImage(file, {
            outputFormat: 'jpg',
            targetWidth: 900,
            // targetHeight: 200,
            // crop: true
        }, (err, b64img) => {
            if (err) {
                console.error(err);
                return;
            } 
            this.urltoFile(b64img, fileName, 'image/jpeg')
            .then(function(resizeFile){
                let uploadInstance = Collections.files.insert({
                  file: resizeFile,         
                  meta: { 
                    userPath:self.state.userPath,          
                    locator: self.props.fileLocator,
                    userId: Meteor.userId() // Optional, used to check on server for file tampering
                  },
                  streams: 'dynamic',
                  chunkSize: 'dynamic',
                  allowWebWorkers: true // If you see issues with uploads, change this to false
                }, false);

                self.setState({
                  uploading: uploadInstance, // Keep track of this instance to use below
                  inProgress: true, // Show the progress bar now
                  loading:true,
                  success:false,
                });

                // These are the event functions, don't need most of them, it shows where we are in the process
                uploadInstance.on('start', function () {

                });

                uploadInstance.on('end', function (error, fileObj) {
                  let photos=self.state.photos;
                  // console.log('On end File Object: ', fileObj);
                  photos.push(fileObj._id);
                  self.setState({
                    photos:photos,
                    bAddPhoto:true,
                  });
                });

                uploadInstance.on('uploaded', function (error, fileObj) {
                  // console.log('uploaded: ', fileObj);   
                });

                uploadInstance.on('error', function (error, fileObj) {
                  console.log('Error during upload: ' + error);
                });

                uploadInstance.on('progress', function (progress, fileObj) {
                  // console.log('Upload Percentage: ' + progress);
                  if(progress === 100){
                    console.log('progress : 100%');
                      self.setState({
                        progress: 0,
                      });
                      Meteor.setTimeout(() => {
                        self.setState({
                          uploading: [],                
                          inProgress: false,
                          loading:false,
                          success:true,
                        }); 
                      }, 0);  
                  }else{
                      // Update our progress bar
                      console.log(progress)
                      self.setState({
                      progress: progress
                    });
                    // console.log('progress : ',progress);
                  }
                });
                uploadInstance.start(); // Must manually start the upload  
            })
        });  
      this.setState({
        loading:true,
      });
    }
  }
}

removeFile(e,id){
  e.preventDefault();
  "use strict";
  const { photos } = this.state;
  // console.log('remove',id);

    Meteor.call('RemoveFile', id, function (err, res) {
      if (err){
        console.log(err);
      }
    });
    photos.splice(photos.indexOf(id),1);
    // console.log(photos);
    this.setState({
      photos:photos,
    });
}

submit(data){
  const {history}=this.props
  data.photos=this.state.photos;
  console.log(data); 

if(this.props.context==='new'){
   Meteor.call('annonces.insert', data,(err, res) => {
    if(err){
      console.log(err)
      let error={};
      error.type=err.errorType
      error.message=err.message

      this.setState({
        errorInsert:true,
        error:error,
      });
    }else if(res){
      console.log(res)
      this.setState({
        snackbar:true,
        tempAddr:'',
        address:"",
        latlng:{},
        returnAddress:'',
        postcode:"",
        photos:[],
        tempValues:{
          type:"Offre",                        
          title:"",                      
          description:"", 
          price:"",                      
          categorie:"",                      
          address:"",                      
          postcode:"",                      
          latlng:"",                      
          photos:"",                      
        },
      });
      Meteor.setTimeout(() => {
        history.push('/mes_annonces'); 
      }, 500);        
    }
  }); 
 }else if(this.props.context==='edit' && this.state.id){
  console.log(this.state.id)
    Meteor.call('annonces.update', this.state.id,data,(err, res) => {
    if(err){
      console.log(err)
      let error={};
      error.type=err.errorType
      error.message=err.message

      this.setState({
        errorInsert:true,
        error:error,
      });
    }else{
      console.log(res)
      this.setState({
        snackbar:true,
      });
      Meteor.setTimeout(() => {
        history.push('/mes_annonces'); 
      }, 500); 
    }
  });
  }  
}

resetScrollToEnd(e){
  this.setState({
     bAddPhoto:false,
  }); 
}

searchPlace(e,context){
  e.preventDefault();
  // console.log(this.state.tempAddr)
  // console.log(this.state.address)
  // console.log(e.keyCode)
  if(e.keyCode === 13 || context === 'MyPos' || context === 'click'){
    // console.log('search')
    this.setState({ 
      address: context === 'MyPos' ? 'MyPos' : this.state.tempAddr,
      search: context === 'MyPos' ? false : true,
    });  
    ReactDOM.findDOMNode(this.refs.submit).focus()
  }
}

getLatLng(e,addr){
  // console.log('return Map',e)
  // console.log(e.components.state) // REGION
  // console.log(e.components.state_district) // DEPARTEMENTS
  console.log(e.name) 
  this.setState({
    address:addr,
    tempAddr:e.name,
    returnAddress:e.name,
    search:false,
    latlng:e.latlng,
    postcode:e.components.postcode ? e.components.postcode : '',
    city:e.components.city ? e.components.city : e.components.village ? e.components.village : 'no City',
  }); 
}

placeFieldClick(e){
  e.preventDefault()
  const{history,location}=this.props
  if(!isWidthUp('md',this.props.width)){
    this.setState({
       height:innerHeight,
       dialog:true,
    });
    history.push(location)
    }else if(e.target.type==='text'){
    console.log(e.target)
    e.target.select()
  }
}

closeDialog(e,c) {
  e.preventDefault();
  // console.log(this.state.oldAddress)
  // console.log(this.state.returnAddress)
  if(c==='cancel'){
    this.setState({     
       city:this.state.tempValues.city,
       postcode:this.state.tempValues.postcode,
       latlng:this.state.tempValues.latlng,
       returnAddress:this.state.oldAddress,
       tempAddr:this.state.oldAddress,
       dialog:false,
    });
  }else if(c==='confirm'){
    this.setState({
       oldAddress:this.state.returnAddress,
       dialog:false,
    });
  }else{
    this.setState({
       dialog:false,
    });
  }
}

validate(model, err, callback){
  if(isLoggedIn()){
    if(err){
      // console.log(model,error)
      this.setState({
         errorValidation:true,
      });     
    } 
    return callback()  
  }else{
    let error={};
    error.type="Erreur"
    error.message="Loguez vous pour poster une annonce !"
    this.setState({
       errorInsert:true,
       error:error,
    }); 
  } 
}

closeErrorDialog(){
  this.setState({
   errorValidation:false,
   errorInsert:false,
  });
}

closeSnackBar(){
  this.setState({
     snackbar:false,
  }); 
}

componentDidUpdate(prevProps){ 
  const { context,location,history } = this.props;
  const self=this

  // console.log(this.state.returnAddress)
  window.onpopstate=function(e){
    // console.log(history.action,location)
    if(history.action==='POP'){        
      self.closeDialog(e,'cancel')
    }
  }

  if(prevProps.context !== context && context === 'new'){
    this.setState({ 
      tempValues:{
        type:"Offre",
        title:"",
        description:"",
        price:"",
        categorie:"",                      
        address:"",                       
        city:"",                       
        postcode:"",                       
        latlng:{},                      
        photos:"",                      
      },  
      latlng:{},    
      postcode:"",
      city:"",
      oldAddress:"",
      tempAddr:"",
      address:"",
      returnAddress:"",
      photos:[],
    });
  }
}

  componentDidMount(){
    const { id }  = this.state;
    if(id){
      this.getAnnonce()
    }    
  }

  getAnnonce(){
    const { id }  = this.state;

    Meteor.call('oneAnnonce.get',id,(err,res) => {
      if (err) {
        console.error('error', err)
      }else{
        console.log(res.props[0].address);
        this.setState({ 
          tempValues:{
            type:res.props[0].type,                      
            title:res.props[0].title,                      
            description:res.props[0].description,                      
            price:res.props[0].price,                      
            categorie:res.props[0].categorie,                      
            address:res.props[0].address,    
            city:res.props[0].city,                   
            postcode:res.props[0].postcode,                   
            latlng:res.props[0].latlng,                      
            photos:res.props[0].photos,                      
          },
          address:res.props[0].address,                      
          oldAddress:res.props[0].address,                      
          returnAddress:res.props[0].address,                      
          tempAddr:res.props[0].address,                      
          city:res.props[0].city,                      
          latlng:res.props[0].latlng,                      
          photos:res.props[0].photos,  
          postcode:res.props[0].postcode,  
        });
      }
   });
  }

  change(field,value){
  const{tempValues}=this.state
  // console.log(field,value)
  switch(field){
    case 'searchaddress':
      this.setState({ tempAddr:value });  
      break;
    case 'categorie':
       tempValues.categorie=value    
       break;
    case 'type':
      tempValues.type=value   
      break;
    case 'title':
      tempValues.title=value   
      break;
    case 'description':
      tempValues.description=value   
      break;
    case 'price':
      tempValues.price=value   
      break;
  }

  this.setState({tempValues:tempValues});  
}

  render() {
    const { classes,userPhotos,width } = this.props;
    const { schema,photos, loading, success,progress,listCat,tempAddr,address,
            latlng,search,dialog,bAddPhoto,returnAddress,
            postcode,city,errorValidation,errorInsert,error,tempValues } = this.state;
    const a=[];
    const aType=schema.get('type','allowedValues');
    let f = Collections.files.find({_id: { $in: photos }}).fetch();
    let formRef;

    // console.log(returnAddress);
    // console.log(tempValues)
    f.map((userPhotos,key) => {
      let link1 = userPhotos.versions.preview ? Collections.files.findOne({_id:userPhotos._id }).link('preview') : null;
      a.push({id:userPhotos._id,link:link1});   
    });

    return (
      <div >
      <Grid container className={classes.root}>
        <Grid item xs={1} sm={1} md={2} className={classes.hideOnSmall}>
          
        </Grid>
 
 
        <Grid item xs={12} sm={12} md={8} >
          <AutoForm
              style={{textAlign:'center'}}
              validate="onSubmit"
              onValidate={(model, err, callback) => this.validate(model, err, callback)}
              ref={ref => formRef = ref}
              schema={schema}
              onChange={(field,value) => this.change(field,value)}
              onSubmit={(data) => this.submit(data)}
              onSubmitSuccess={() => formRef.reset() }
              >
              {
                //////////       GALLERY     //////////////////
              }
              <LineGridList
                  style={{marginTop:20}}
                  tileData={a} 
                  rmFile={((e,id) => this.removeFile(e,id))}
                  resetScrollToEnd={((e) => this.resetScrollToEnd(e))}
                  bAddPhoto={bAddPhoto}
                  />
              {
              //////////      INPUT PHOTOS     //////////////////
              }
              <div className={classes.wrapper}>
                <input id="fileinput" 
                       disabled={loading} 
                       accept="image/*" 
                       type="file"
                       onChange={((e) => this.uploadIt(e))}/> 
                <label htmlFor="fileinput"> 
                <Fab
                  color="primary"
                  component="span"
                  disabled={loading}> 
                  {(progress !== 0) ? 
                    progress + '%'
                   : <PhotoCamera  fontSize="large" />  
                  }
                </Fab>
                <CircularProgress 
                  color="primary"
                  size={68} 
                  variant="static" 
                  value={progress}
                  className={classes.progress} />
                </label> 
                
              </div>  
              {
                //////////      CATEGORIE     //////////////////
              }
              <TextField
                margin="dense"
                style={{marginTop:50}}
                value={tempValues.categorie}
                InputLabelProps={{ shrink:tempValues.categorie==="" || tempValues.categorie===undefined ? false : true}}
                ref="inputCat"
                variant="outlined"
                name="categorie"                
                id="categorie"                
                select
                className={classes.field}
              >
                {
                  listCat.map((c,key )=> (                        
                  <MenuItem 
                    disabled={c[1].startsWith("--") ? true : false} 
                    key={key}
                    value={c[0]}>
                    {c[1]}
                  </MenuItem>
                ))}
              </TextField>
              {
              //////////      TYPE D'OFFRE     //////////////////
              <div>
                <TextField
                  margin="dense"
                  value={tempValues.type}
                  InputLabelProps={{ shrink:tempValues.type==="" || tempValues.type===undefined ? false : true}}
                  variant="outlined"
                  name="type"                    
                  id="type"                    
                  select
                  className={classes.field}
                >
                  {
                    aType.map((t,key )=> (                        
                    <MenuItem 
                      key={key}
                      value={t}>
                      {t}
                    </MenuItem>
                  ))}
                </TextField>              
              </div>  
              //////////       TITLE     //////////////////
              }
              <TextField 
                  margin="dense"   
                  className={classes.field}
                  type="text"
                  InputLabelProps={{ shrink:tempValues.title==="" || tempValues.title===undefined ? false : true,classes:{root:classes.blue}}}
                  value={tempValues.title}  
                  name="title" 
                  // style={{marginTop:50}}
                  />
              <LongTextField 
                  margin="dense"
                  className={classes.field}
                  type="text"
                  InputLabelProps={{ shrink:tempValues.description==="" || tempValues.description===undefined ? false : true}}
                  value={tempValues.description}
                  name="description" /> 
              <TextField 
                  margin="dense"
                  className={classes.field}
                  type="number"
                  InputLabelProps={{ shrink:tempValues.price==="" || tempValues.price===undefined ? false : true}}
                  value={tempValues.price}
                  name="price" /> 
              {
                //////////       LOCALISATION     //////////////////              
              }
              {
                // <Divider variant="middle" className={classes.divider} />
              }
                <HiddenField 
                    value={latlng}
                    name="latlng" />
                <HiddenField 
                    value={postcode}
                    name="postcode" />
                <HiddenField 
                    value={city}
                    name="city" />
                <HiddenField 
                    value={returnAddress}
                    name="address" />
                <CoreTextField    
                    variant="outlined"
                    margin="dense"
                    ref="inputAddr"
                    value={tempAddr}                  
                    placeholder={isWidthUp('md',width) ? "Chercher une adresse ou cliquer sur la carte..." : "Localiser mon annonce"}
                    onClick={(e) => this.placeFieldClick(e)}
                    onChange={(e) => this.change('searchaddress',e.target.value)}
                    onKeyUp={(e) => this.searchPlace(e)}
                    className={`${classes.field} ${classes.searchPlacefield}`}   
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start"  className={classes.hideOnSmall}>
                          <IconButton onClick={(e) => this.searchPlace(e,'MyPos')}>
                             <LocationIcon  color="primary"  />
                          </IconButton>  
                          <Divider className={`${classes.inputDivider} ${classes.hideOnSmall}`}/>                       
                        </InputAdornment>
                      ),endAdornment: (
                        <InputAdornment position="end"  className={classes.hideOnSmall}>
                          <IconButton onClick={(e) => this.searchPlace(e,'click')}>
                             <SearchIcon  color="primary"  />
                          </IconButton>                        
                        </InputAdornment>
                      ),
                    }}               
                    />  
              {
                //////////       MAP     //////////////////
              }           
              <div className={`${classes.map} ${classes.hideOnSmall}`} >
                <LocateMap   
                    clickable={true}
                    address={address} 
                    latlng={latlng} 
                    search={search}                         
                    returnLatLng={(e) => this.getLatLng(e)}
                    /> 
              </div>
              {
                //////////       MODAL     //////////////////
              }
              <Dialog 
                fullScreen
                open={dialog}
                onClose={(e) => this.closeDialog(e,'cancel')}
                TransitionComponent={Transition}
              >
              {
                returnAddress && returnAddress.length > 0 ?
                <Paper className={classes.zoomPaperReturnAddress} elevation={1}>
                  <Typography component="p">
                    {returnAddress}
                  </Typography>
                </Paper> : ''}

                <Fab size="small" className={classes.fabLocation}>
                  <LocationIcon  onClick={(e) => this.searchPlace(e,'MyPos')}/>
                </Fab>

              {
              <AppBar position="static">     
                <Toolbar className={classes.toolbar}>
                  <IconButton  color="inherit" onClick={(e) => this.closeDialog(e,'cancel')} >
                    <ArrowLeft />
                  </IconButton>

                  <div className={classes.search}>
                    <InputBase
                      onKeyUp={(e) => this.searchPlace(e)}
                      onChange={(e) => this.change('searchaddress',e.target.value)}                      
                      placeholder="Search…"
                      classes={{ 
                        root: classes.inputRoot, 
                        input: classes.inputInput, 
                      }} 
                      endAdornment= 
                      {<InputAdornment position="end">
                        <IconButton style={{color:'white'}}  onClick={(e) => this.searchPlace(e,'click')}>
                           <SearchIcon />
                        </IconButton>                        
                      </InputAdornment>}                        
                    />
                  </div>
                             
                  <IconButton  color="inherit" onClick={(e) => this.closeDialog(e,'confirm')}>
                    <CheckIcon />
                  </IconButton>               
                
                </Toolbar>
              </AppBar>
              ////////////       MAP MODAL     //////////////////
              }              
              <LocateMap                     
                  clickable={true}
                  address={address} 
                  latlng={latlng} 
                  search={true} 
                  returnLatLng={(e) => this.getLatLng(e)}/> 
              </Dialog>
              {
              //////////       ERROR DIALOG     //////////////////
              }
              <Dialog
                open={errorValidation || errorInsert}
              >
                <DialogTitle>
                  {errorValidation &&
                    "Veuillez corriger ces erreurs :"
                  }
                  {errorInsert &&
                    error.type
                  }
                </DialogTitle>
                           
                <DialogContent>
                  {errorValidation &&
                    <ErrorsField />
                  }
                  {errorInsert &&
                    error.message}
                </DialogContent>
                <DialogActions>
                  <Button onClick={(e) => this.closeErrorDialog(e)} color="primary" autoFocus>
                    OK
                  </Button>
                </DialogActions>
              </Dialog>

              {
                //////////       SUBMIT     //////////////////
              }
              <div style={{textAlign:'center'}}>
                <Button 
                    size="large"
                    disabled={loading} 
                    variant="contained"
                    color="primary"
                    onClick={() => formRef.submit()}
                    ref="submit"  
                    className={classes.submitButton}>Valider
                </Button> 
              </div>     
          </AutoForm>
              {
                //////////       SNACKBAR     //////////////////
              } 
              <Snackbar
                open={this.state.snackbar}
                autoHideDuration={3000}
                onClose={(e) => this.closeSnackBar(e)}
              >
                <SnackbarContent
                  message={"Annonce enregistre !"}
                /> 
              </Snackbar>
        </Grid>
        <Grid item xs={1} sm={1} md={2} className={classes.hideOnSmall}>
          
        </Grid>
        </Grid>
      </div>
    );
  } 
}

DepotAnnonce.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default compose(
  withWidth(),
  withStyles(styles),
  withTracker(() => {
  Meteor.subscribe('files.all');
  return {
      userPhotos:Collections.files.find().fetch(),
  }
}),
)(withRouter(DepotAnnonce));