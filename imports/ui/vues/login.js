import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import { withRouter, Link } from 'react-router-dom';
import {Tracker} from 'meteor/tracker';
import SimpleSchema from 'simpl-schema';

import Grid from '@material-ui/core/Grid';
import React from 'react';
import ReactDOM from 'react-dom';
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import {AutoForm,AutoField, TextField,ErrorField, ErrorsField} from 'uniforms-material';

const styles = theme => ({
  root: {
    flexGrow: 1,
    width:'99%',
    margin:'auto',
  },
  hideOnSmall:{
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
    // [theme.breakpoints.up('md')]: {
    //  display: 'block',
    // },
    // [theme.breakpoints.up('lg')]: {
    //  display: 'block',
    // },
  },
  dialog:{
    textAlign:'center',
  },
  button: {
    marginTop:25,
    textTransform:'none',
    width:'100%',
  },
});

const signupSchema=new SimpleSchema({
  email:{
    type:String,
    regEx: SimpleSchema.RegEx.Email
  },
  password:{
    type:String,
  } ,
  confirmPassword:{
    type:String,
    custom() {
      if (this.value !== this.field('password').value) {
        return "diffPassword";
      }
    },  
  }
});

const loginSchema=signupSchema.pick('email','password')

const emailSchema=signupSchema.pick('email')

const resetSchema=signupSchema.pick('password','confirmPassword')


class Login extends React.Component {
  constructor(props) {
    super(props);
    const { match, history } = this.props;
    const token = (match && match.params && match.params.token) || null; 

    this.state = {
      schema:loginSchema,
      myview:this.props.myview === 'reset' ? 'reset' : 'login',  
      token:token,
    };
  }

  componentDidMount(){
    this.changeView(this.state.myview)
  }

  secondaryButton () {  
    if(this.state.myview == 'login'){
      this.changeView('signup');
    }else if(this.state.myview == 'signup'){
     this.changeView('login');
    } else if(this.state.myview == 'forgot'){
     this.changeView('login');
    }
  }

  toggleForgot () {  
    this.changeView('forgot');      
  }

  closeAlert () {      
    this.changeView('login');      
  }

  changeView = (view,alertTitle) => {
    let myview='',schema='',title='',secondaryButton='',primaryButton='';
    switch(view){
      case 'reset':
        schema=resetSchema
        myview='reset'
        title="Réinitialisation du mot de passe";
        primaryButton="Réinitialiser mon mot de passe";
        secondaryButton="";
        break;
      case 'signup':
        schema=signupSchema,
        myview='signup';
        title="Créer un compte";
        primaryButton="Je crée mon compte";
        secondaryButton="Déjà enregistré ? Connectez vous";
        break;
      case 'login':
        schema=loginSchema
        myview='login';
        title="Connectez vous";
        primaryButton="Se connecter";
        secondaryButton="Pas encore enregistré ? Créer un compte";
        break;
      case 'forgot':
        schema=emailSchema,
        myview='forgot';
        title="Mot de passe oublié";
        primaryButton="Réinitialiser mon mot de passe";
        secondaryButton="Annuler";
        break;      
      case 'alert':
        schema=null,
        myview='alert';
        title=alertTitle;
        primaryButton="Ok";
        secondaryButton="";
        break;
    } 
    this.setState({
      schema:schema,
      myview: myview,
      title: title,
      primaryButton:primaryButton,
      secondaryButton:secondaryButton,
    });   
  }

  submit(data){
    // console.log(data)  
    const { history } = this.props;    
    const { token } = this.state;    
    const email=data.email
    const password=data.password

      if (this.state.myview === 'signup'){
        Accounts.createUser( {email, password}, ( error ) => {
          if ( error ) {
            console.log( 'error create user', error.reason );
          }else{
            this.changeView('alert','Email de vérification envoyé')
          }
        });

      }else if(this.state.myview === 'login'){    
          Meteor.loginWithPassword(email, password, ( error ) => {
          if ( error ) {
            console.log('error login : ', error.reason );
            this.changeView('alert',error.reason)
          }else{             
            console.log( 'login' );
            history.push('/Home'); 
          }
        });
      }else if(this.state.myview === 'forgot'){
         Accounts.forgotPassword( {email}, ( error ) => {
            if ( error ) {
              console.log( 'error forgotPassword', error.reason );
            }else{
              this.changeView('alert','Email de réinitialisation envoyé')
            }          
        });      
      }else if(this.state.myview === 'reset'){
         Accounts.resetPassword( token, password, ( error ) => {
          if ( error ) {
            console.log( 'error reset password', error.reason );
            this.changeView('alert',error.reason)
          }else{
            this.changeView('alert','Mot de passe réinitialisé')
          }       
        });
      }
  }

  render() {
    const { classes,theme } = this.props;
    const { schema,token ,myview,title,primaryButton,secondaryButton }= this.state;

    console.log(myview)
    let formRef;
   
    return ( 

          <Dialog
            fullScreen
            className={classes.dialog}
            open={true}
            onClose={this.handleClose}
            aria-labelledby="form-dialog-title"
          >
              <Grid container className={classes.root}>
                <Grid item  md={3} lg={4} className={classes.hideOnSmall}>
                  
                </Grid>

                <Grid item xs={12} sm={12} md={6} lg={4}>
                  <DialogTitle id="form-dialog-title">{title}</DialogTitle>
                  {this.state.myview == 'alert' ? 
                  <DialogContent> 
                     <Button 
                      className={classes.button}
                      variant="contained"                   
                      color='primary'                           
                      onClick={() => this.closeAlert()} 
                      >             
                      {primaryButton}
                     </Button>    
                  </DialogContent>
                  :
                  <DialogContent>   
                    <AutoForm
                        style={{textAlign:'center'}}
                        validate="onSubmit"
                        ref={ref => formRef = ref}
                        schema={schema}
                        onSubmit={(data) => this.submit(data)}
                        onSubmitSuccess={() => formRef.reset() }
                        >   

                      {schema._schema.email &&
                      <div>
                        <TextField 
                            margin="dense"   
                            type="text" 
                            name="email" 
                            style={{marginTop:50}}
                            />
                        <ErrorField
                          name="email"
                        />
                      </div>
                      } 
                      {schema._schema.password &&
                      <div>
                        <TextField 
                            margin="dense"   
                            type="password" 
                            name="password" 
                            />
                        <ErrorField
                          name="password"
                        />
                      </div>
                      } 
                      {schema._schema.confirmPassword &&
                      <div>
                        <TextField 
                            margin="dense"   
                            type="password" 
                            name="confirmPassword" 
                            />
                        <ErrorField
                          name="confirmPassword"
                        />
                      </div>
                      } 

                      <Button         
                        className={classes.button}
                        variant="contained"                   
                        color='primary'       
                        ref="submit"    
                        onClick={() => formRef.submit()}    
                        >             
                        {primaryButton}
                      </Button>       

                      {myview !== 'reset' &&                 
                      <Button 
                        variant="outlined" 
                        color="secondary"
                        className={classes.button}
                        onClick={() => this.secondaryButton()} 
                        >
                        {secondaryButton}
                      </Button>} 

                      {(myview === 'login' || myview === 'signup') &&
                      <div>                  
                        <Button                    
                          color="secondary"
                          className={classes.button}
                          onClick={() => this.toggleForgot()} >
                          Mot de passe oublié ?
                        </Button>                    
                        <Button   
                          component={Link} 
                          to="/Home"                 
                          color="secondary"
                          className={classes.button}
                          >
                        Annuler
                       </Button> 
                      </div> }  
                    </AutoForm>             
                  </DialogContent>
                }   
            </Grid>


            <Grid item md={3} lg={4} className={classes.hideOnSmall}>
              
            </Grid>
          </Grid>        
        </Dialog>

    );
  }
}

export default withRouter(withStyles(styles)(Login));