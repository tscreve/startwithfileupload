import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import { withRouter, Link } from 'react-router-dom';

import React from 'react';
import ReactDOM from 'react-dom';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  dialog:{
    textAlign:'center',
  },
  formControl: {
    margin: theme.spacing.unit,
    width:'80%'
  },
  button: {
    margin: theme.spacing.unit,
    width:'60%'
  },
});

class VerifyEmail extends React.Component {
  constructor(props) {
    super(props);
    const { match, history } = this.props;    
    const token = (match && match.params && match.params.token) || ''; 

    this.state = { 
      token:token,
    };
  }

  verify () {  
    // console.log(this.props);
    const { history } = this.props;  
    const { token } = this.state;

    Accounts.verifyEmail(token, ( error ) => {
      if ( error ) {
        console.log('error verifyEmail : ', error.reason );
      }else{
        this.setState({
          show: !this.state.show
        }); 
        history.push('/Home'); 
      }
    });  
  }

    
  render() {
    const { classes } = this.props;
   
    return ( 
        <div className={classes.container}>
          <Dialog
          fullScreen
          className={classes.dialog}
            open={true}
            onClose={this.handleClose}
            aria-labelledby="form-dialog-title"
          >
            <DialogTitle id="form-dialog-title">Verify your email</DialogTitle>
            <DialogContent>  
               <Button 
                size="large"
                className={classes.button}
                variant="contained"                   
                color='primary'              
                onClick={() => this.verify()} >             
                Verify
               </Button>    
            </DialogContent>
           
          </Dialog> 
        </div> 
    );
  }
}

export default withRouter(withStyles(styles)(VerifyEmail)) ;
