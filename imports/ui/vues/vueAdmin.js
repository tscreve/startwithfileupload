import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { withRouter, NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import { withTracker } from 'meteor/react-meteor-data'; 
import compose from 'recompose/compose';


import { Annonces } from '../../api/annonces.js';

import withWidth,{ isWidthUp } from '@material-ui/core/withWidth';
import VitrtualTable from '../comps/virtualizedTable.js'
// import VitrtualTable from './example.js'

import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Fab from '@material-ui/core/Fab';

import Eye from '@material-ui/icons/Visibility';
import EyeOff from '@material-ui/icons/VisibilityOff';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ScreenRotation from '@material-ui/icons/ScreenRotation';


// const isAdmin = () => {
//   return Roles.userIsInRole(Meteor.userId(),['admin']); 
// };

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  grow: {
    flexGrow: 1,
  },
  fab:{
    position:'absolute',
    top:20,
    right:30,
  },
  hideOnSmall:{
    [theme.breakpoints.down('md')]: {
      display: 'none',
    },
    // [theme.breakpoints.up('md')]: {
    //  display: 'block',
    // },
    // [theme.breakpoints.up('lg')]: {
    //  display: 'block',
    // },
  },
  title:{
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
    textAlign: 'center',
    backgroundColor: 'white',
    lineHeight: 3,
    margin: 0,
  },
});

class vueAdmin extends Component {
  constructor(props) {
    super(props);

    this.getOrientation=this.getOrientation.bind(this)
    this.state = {
      tabValue:0,
    };    
  }

  componentDidUpdate(){

  }

  componentDidMount() {
    this.getOrientation()
    window.addEventListener("resize", this.getOrientation,false);
  }

 componentWillUnmount() {
    window.removeEventListener("resize", this.getOrientation,false);
  }

  changeTab = (event, value) => {
    this.setState({ tabValue:value });
  }

  getOrientation() {
    // console.log(screen.orientation)
    this.showBar()
    this.setState({ 
        orientation:screen.orientation.angle===0 ? 'p' : 'l'
      });
  }

  hideBar = () => {
    this.setState({ hideBar:true });
    document.getElementById('MyAppBar').style.display = "none";
  }

  showBar = () => {
    this.setState({ hideBar:false });
    document.getElementById('MyAppBar').style.display = "block";
  }

  render() {
    const { classes,ann,users,width } = this.props;
    const { tabValue,orientation,hideBar } = this.state;
    // console.log(orientation)
    // const isAdmin=Roles.userIsInRole(Meteor.user(),['admin'])
    // console.log(isAdmin());

    // console.log(this.props.users)
    // console.log(this.props.ann)
    return (
    <div>
      <Grid container  className={classes.root}>
        <Grid item lg={1}  xl={1} className={classes.hideOnSmall}>
         
        </Grid>
          <Grid item xs={12} sm={12} md={12} lg={10} xl={10} style={{backgroundColor:'white'}}>  

          <h2 className={classes.title}  style={{display:(orientation==='p'  && !isWidthUp('sm',width) || hideBar) ? 'none' : 'block'}}>Gestion des
            {tabValue === 0 && " annonces"}
            {tabValue === 1 && " utilisateurs"}
            {tabValue === 2 && " messages"}
          </h2>

          <div style={{display:(orientation==='p' && !isWidthUp('sm',width)) ? 'block' : 'none',textAlign:'center',marginTop:(innerHeight-182)/2}} >
            <ScreenRotation style={{fontSize:80}} />
            <p>Passez en mode paysage afin de profiter de l'interface d'administration</p>
          </div>

            <AppBar position="static" style={{display:(orientation==='p'  && !isWidthUp('sm',width) || hideBar) ? 'none' : 'block'}}>
            <Toolbar>
              <Tabs value={tabValue} onChange={this.changeTab}>
                <Tab label="Annonces" />
                <Tab label="Utilisateurs" />
                <Tab label="Messages" />  
              </Tabs>
              <div className={classes.grow} /> 
               {!isWidthUp('md',width) &&
                <IconButton 
                 color="inherit" 
                 onClick={this.hideBar}
                >
                 <EyeOff/>
                </IconButton>}
            </Toolbar>
            </AppBar>
            <div style={{display:(orientation==='p'  && !isWidthUp('sm',width)) ? 'none' : 'block'}}>
              {tabValue === 0 && ann.length > 0 && <VitrtualTable context={'ann'}  data={ann} fullHeight={hideBar} width={width} />}
              {tabValue === 1 && users.length > 0 && <VitrtualTable context={'users'}  data={users} fullHeight={hideBar} width={width} />}
            {  
              // tabValue === 2 && <VitrtualTable data={ann} />
            }
            </div>

            {
              hideBar &&
             <Fab               
              className={classes.fab}               
              color="primary"
              size="small"
              onClick={this.showBar}>
              <ExpandLess />
            </Fab>             
            }



          </Grid>
        <Grid item lg={1}  xl={1} className={classes.hideOnSmall}>
         
        </Grid>
      </Grid>
    </div>

    );
  } 
}

vueAdmin.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default compose(
  withStyles(styles),
  withWidth(),
  withTracker(() => {

  Meteor.subscribe('annonces.all');
  Meteor.subscribe('users.all');
  return {
      ann: Annonces.find().fetch(),
      users: Meteor.users.find().fetch(),
  }
}),
)(vueAdmin);