import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { withRouter, NavLink,Link } from 'react-router-dom';
import compose from 'recompose/compose';
import { Session } from 'meteor/session';

import MyCat from '../../api/categorie.js'
import MyRegion from '../../api/region.js'

import Zoom from '../comps/zoom.js';
import SimpleMap from '../comps/simpleMap.js';
import Swiper from 'react-id-swiper';

import withWidth,{ isWidthUp } from '@material-ui/core/withWidth';
import Paper from '@material-ui/core/Paper';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import { withStyles } from '@material-ui/core/styles';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Card from '@material-ui/core/Card';
import Slide from '@material-ui/core/Slide';
import CircularProgress from '@material-ui/core/CircularProgress';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import ZoomTransition from '@material-ui/core/Fade';

import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';

import RestoreIcon from '@material-ui/icons/Restore';
import FavoriteIcon from '@material-ui/icons/Favorite';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import MessageIcon from '@material-ui/icons/Forum';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  hideOnSmall:{
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
    // [theme.breakpoints.up('md')]: {
    //  display: 'block',
    // },
    // [theme.breakpoints.up('lg')]: {
    //  display: 'block',
    // },
  },
  hideOnXSmall:{
    [theme.breakpoints.down('xs')]: {
      display: 'none',
    },
  },
  hideOnLarge:{
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  path:{
    marginLeft:20,
  },
  progressContainer:{
    textAlign:'center',
  },
  images:{
    position:'relative',
    maxWidth:'100%',
    margin:'auto',
    display:'block',
  },
  bigImages:{
    boxShadow: '2px 10px 13px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14)'
  },
  imageBckgnd:{
    height: 500,
    opacity: 0.1,
    position: 'absolute',
    width: '100%',
    backgroundSize: 'cover',
  },
  imagesContainer:{
    width:'120px!important',
    height:'100px!important',
    opacity:0.5,
  },
  tabs:{
    backgroundColor:'#e0e0e0',
  },
  gallerySwiper:{
    backgroundColor:'#e0e0e0',
  },
  title:{
    overflowWrap:'break-word',
  },
  bottomBar:{
    top: 'auto',
    bottom: 0,    
  },
  bottomNav:{
    backgroundColor:theme.palette.primary.main,
    color:"white",
    width:'100%',
  },
  bottomButton:{
    color:'white',
    maxWidth:'unset',
  },
  divBottomNav:{
    backgroundColor:'white',
    height:64,
    [theme.breakpoints.down('xs')]: {
      height:56,
    },
  },
});


class Annonce extends Component {
  constructor(props) {
    super(props);

    const { match } = this.props;
    const id = (match && match.params && match.params.id) || null;
    // console.log(this.props.history);   

    this.state = {
      gallerySwiper: null,
      thumbnailSwiper: null,
      id: id,
      idGallery:0,
      openZoom:false,
      subBar:false,
      bottomNav:0,
      categorie:{
        type:'',
        current:[],
        main:[],
        sub:[],
      },
    };

    // this.getScroll=this.getScroll.bind(this)
    this.galleryRef = this.galleryRef.bind(this);
    this.thumbRef = this.thumbRef.bind(this);
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextState.gallerySwiper && nextState.thumbnailSwiper ) {
      const { gallerySwiper, thumbnailSwiper } = nextState

      gallerySwiper.controller.control = thumbnailSwiper;
      thumbnailSwiper.controller.control = gallerySwiper;
    }
  }


  galleryRef(ref) {
    if (ref) this.setState({ gallerySwiper: ref.swiper})
  }

  thumbRef(ref) {
    if (ref) this.setState({ thumbnailSwiper: ref.swiper })
  }

  getScroll = (e) => {
    e.preventDefault();
    const subBar=window.scrollY > 250 ? true : false;
    // console.log(scrollY)
    // console.log(Session.get('scroll'))
    this.setState({ subBar: subBar})
  }

  componentDidUpdate(){
  }

  componentDidMount(){
    // window.addEventListener("scroll",this.getScroll,false);
    // window.scrollTo(0,0)
    this.getAnnonce();
  }

  componentWillMount(){
  }

  componentWillUnmount() {
    // window.removeEventListener("scroll", this.getScroll,false);
  }

  getAnnonce(){
    const { id }  = this.state;

    Meteor.call('oneAnnonce.get',id,(err,res) => {
      if (err) {
        console.error('error', err)
      }else{
        // console.log(res);
        this.setState({ 
          categorie:{
            type:res.props[0].type,
            current:res.cat.currentCat,
            main:res.cat.mainCat, 
            sub:res.cat.subCat,                       
          },
          item:res,
          latlng:res.props[0].latlng,
        });
      }
   });
  }

  handleClose = () => {
    this.setState({ openZoom: false });
    document.getElementById('MyAppBar').style.display = "block";
  };

  zoom(){
    this.setState({
      openZoom:true,
    });
    document.getElementById('MyAppBar').style.display = "none";
  }

  change(){
    this.setState({
      idGallery:this.state.gallerySwiper.activeIndex,
    });
    // console.log(this.state.gallerySwiper.activeIndex)
  }
 
  render() {
    const gallerySwiperParams = {
      on: {
        slideChange:() => this.change(),
      },
      spaceBetween: 10,
      pagination: !isWidthUp('sm',this.props.width) && {
        el: '.swiper-pagination',
        clickable: true,
        renderBullet: (index, className) => {
          return '<span class="' + className + '">' + (index + 1) + '</span>';
        },
      },
    };

    const thumbnailSwiperParams = {
      spaceBetween: 0,
      centeredSlides: true,
      slidesPerView: 5,
      touchRatio: 0.8,
      slideToClickedSlide: true,
    }

    const { classes }  = this.props;
    const { item,openZoom,subBar,latlng,idGallery,categorie }  = this.state;
    // console.log(item)
    let imgLinks,id=null;
    
    if(item){
      imgLinks=item.link;
      id=item.props[0]._id
      // console.log(item.a)
    }
    console.log(imgLinks)
    return (
    <div className={classes.root} >  
      {imgLinks && openZoom &&
        <div style={{display: openZoom ? 'block' : 'none' }} >
          <Zoom 
            id={idGallery} 
            item={imgLinks} 
            // returnId={(e) => this.returnIdFromZoom(e)}
            handleClose={(e) => this.handleClose(e)}/>
        </div>}

    <div style={{display: !openZoom ? 'block' : 'none' }}>
    {
      subBar &&
      <Slide direction="down" in={true} mountOnEnter unmountOnExit>
        <AppBar position="fixed"  >
          <Toolbar>
            <Grid container>
              <Grid item md={2} className={classes.hideOnSmall}>
              </Grid>

              <Grid item xs={12} sm={12} md={8} style={{display:'flex'}}>
                <IconButton  color="inherit" aria-label="Menu">
                  <MenuIcon />
                </IconButton>              
                <Typography variant="h6" color="inherit" style={{flexGrow:1}}>
                  News
                </Typography> 
                <Button color="inherit">Login</Button>
              </Grid>

              <Grid item md={2} className={classes.hideOnSmall}>
              </Grid>
            </Grid>
           </Toolbar>
          </AppBar>
        </Slide>
      }


      <Grid container >
        <Grid item md={2} className={classes.hideOnSmall}>
        </Grid>
 
 
        <Grid item xs={12} sm={12} md={8} >

          <Card >
            <p className={classes.path}>
              <Link to={'/'+categorie.type}>{categorie.type+' > '}</Link>
              {
                categorie.type === 'Offre' ?
                <span>
                  <Link to={'/offres/categorie/'+categorie.main[0]+','+categorie.sub}>{categorie.main[1]+' > '}</Link>
                  <Link to={'/offres/categorie/'+categorie.main[0]+','+categorie.current[0]}>{categorie.current[1]}</Link>
                </span>
              :
                <span>
                  <Link to={'/demandes/categorie/'+categorie.main[0]+','+categorie.sub}>{categorie.main[1]+' > '}</Link>
                  <Link to={'/demandes/categorie/'+categorie.main[0]+','+categorie.current[0]}>{categorie.current[1]}</Link>
                </span>
              }
            </p> 

        <div style={{height: innerWidth > 500 ? 500 : innerWidth}}> 
        {
          imgLinks  ?
             <ZoomTransition 
                in={true}
                style={{transitionDelay: '200ms' }} 
                mountOnEnter 
                unmountOnExit>
                <div 
                  className={classes.gallerySwiper} id="mainGallery" >
                  <Swiper {...gallerySwiperParams} 
                          ref={this.galleryRef} 
                          >
                    {imgLinks.map((l,key) => (
                      <div 
                        key={l[0]} 
                        className={classes.tabs}
                        >
                        <div key={"back" + l[0]} className={classes.imageBckgnd} style={{backgroundImage:'url("' + l[0] + '")'}}></div>     
               
                          <img                            
                            onClick={(e) => this.zoom(e)}                             
                            className={`${classes.bigImages} ${classes.images}`} 
                            src={l[0]}
                          />                      
                      </div>
                    ))}
                  </Swiper>
                </div>
              </ZoomTransition>
              :
              <div className={`${classes.progressContainer} ${classes.hideOnLarge}`} style={{marginTop:innerWidth/2}}>
                <CircularProgress />   
              </div> 
          }
          </div>   

          <div style={{height:100,marginTop:20}} className={classes.hideOnXSmall}>
          {imgLinks ?
                          <div 
                          className={classes.thumbSwiper}>
                          <Swiper  {...thumbnailSwiperParams}  
                                    ref={this.thumbRef}
                                    >
                            {imgLinks.map((l,key) => (
                              <div  
                                className={classes.imagesContainer} 
                                key={l[1]} >
                                  <img 
                                    className={classes.images}
                                    src={l[1]}
                                  />                        
                              </div>
                            ))}
                          </Swiper>
                        </div>
                        :
                        <div className={classes.progressContainer}>
                          <CircularProgress />   
                        </div> 
                      }
            </div>

            <CardContent>
              <Typography gutterBottom variant="h5" component="h2" className={classes.title}>
                {item && item.props[0].title} 
              </Typography>
              <Typography component="p">
                {item && item.props[0].description}
              </Typography>
              <Typography gutterBottom variant="h4" component="h2">
                {item && item.props[0].price} 
              </Typography>

            </CardContent>

            <CardActions>
              <Button size="small" color="primary">
                Share
              </Button>
              <Button size="small" color="primary">
                Learn More
              </Button>
            </CardActions>

          {imgLinks &&
                      <div style={{height:200}}>
                        <SimpleMap latlng={latlng}/>            
                      </div> }
          </Card>



          <div className={classes.divBottomNav}>
            <AppBar position="fixed" color="primary" className={classes.bottomBar}>
              <Toolbar 
                >
                <BottomNavigation
                  showLabels
                  className={classes.bottomNav}
                >
                  <BottomNavigationAction 
                      label="Recents" 
                      className={classes.bottomButton} 
                      icon={<RestoreIcon />} />

                  {id && item.props[0].createBy !== Meteor.userId() &&
                    <BottomNavigationAction 
                    to={{
                      pathname:"/messages/",
                      state:{
                        objectId:id
                      }
                    }}
                    component={Link} 
                    label="Contacter" 
                    className={classes.bottomButton} 
                    icon={<MessageIcon />} />}

                  {id &&
                    <BottomNavigationAction 
                      to={"/modifier_une_annonce/" + id}
                      component={Link}  
                      label="Modifier" 
                      className={classes.bottomButton} 
                      icon={<LocationOnIcon />} />}
                </BottomNavigation>
              </Toolbar>
            </AppBar>
          </div>                       
        </Grid>

        <Grid item md={2} className={classes.hideOnSmall}>
        </Grid>
      </Grid>
    </div> 
    </div>
    );
  } 
}
export default compose(
    withWidth(),
    withStyles(styles),
)(withRouter(Annonce));