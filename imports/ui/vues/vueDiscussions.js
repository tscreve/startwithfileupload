import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { withRouter, NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import { withTracker } from 'meteor/react-meteor-data'; 
import compose from 'recompose/compose';

import { Discussions } from '../../api/discussions.js';

import ListComp from '../comps/listComp.js';

import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    flexGrow: 1,
    // marginTop:40,
  },
  hideOnSmall:{
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
    // [theme.breakpoints.up('md')]: {
    //  display: 'block',
    // },
    // [theme.breakpoints.up('lg')]: {
    //  display: 'block',
    // },
  },
});

class ListDiscussions extends Component {
  constructor(props) {
    super(props);   

    // this.state = {

    // };
  }

  componentDidMount(){

  }

  componentDidUpdate(prevProps){

  }

  render() {
    const { classes,discussions } = this.props;

    return (
    <div>
      <Grid container  className={classes.root}>
        <Grid item xs={1} sm={1} md={2} className={classes.hideOnSmall}>
         
        </Grid>

        
          <Grid item xs={12} sm={12} md={8}>  
            <ListComp items={discussions} context="discussions"/>
          </Grid>


        <Grid item xs={1} sm={1} md={2} className={classes.hideOnSmall}>
         
        </Grid>
      </Grid>
    </div>

    );
  } 
}

ListDiscussions.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default compose(
  withStyles(styles),
  withTracker(() => {
  Meteor.subscribe('discussions.all');
  return {
      discussions: Discussions.find({},{sort:{lastModified:-1}}).fetch(),
  }
}),
)(ListDiscussions);