import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { withRouter, NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import { withTracker } from 'meteor/react-meteor-data'; 

import MyCat from '../../api/categorie.js'
import MyRegion from '../../api/region.js'

import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';

import LocateMap from '../comps/map.js';

import SearchListComp from '../comps/searchListComp.js'


const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  hideOnSmall:{
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
    // [theme.breakpoints.up('md')]: {
    //  display: 'block',
    // },
    // [theme.breakpoints.up('lg')]: {
    //  display: 'block',
    // },
  },
progress: {
    margin: 40,
  },
});

class VueListeAnnonces extends Component {
  constructor(props) {
    super(props);

    const { match,history } = this.props;
    const cat = (match && match.params && match.params.categorie) || null;
    const region = (match && match.params && match.params.region) || null;

    // console.log(this.props)
    this.state = {
      type: this.props.type,
      cat: cat,
      region: region,
      scroll:0,
    };    
  }

  componentWillUpdate(prevProps){  

  }

  componentDidUpdate(prevProps){  
    const { match,history } = this.props;
    const cat = (match && match.params && match.params.categorie) || null;
    // console.log(prevProps.match)
    // console.log(this.props.match)
    if(this.state.type !== this.props.type){    
        this.setState({type:this.props.type}); 
    }else if(this.state.cat !== cat){ 
        this.setState({cat:cat}); 
    }
  }

  render() {
    const { classes } = this.props;
    const { type,cat,region } = this.state;
    // console.log('render',type);
    // console.log(cat);

    return (
    <div className={classes.root}>
        <Grid container  className={classes.root}>
          <Grid item md={2} className={classes.hideOnSmall}>
           
          </Grid>


          <Grid item xs={12} sm={12} md={8}>  
            <SearchListComp type={type} cat={cat} region={region} />
          </Grid>


          <Grid item md={2} className={classes.hideOnSmall}>
           
          </Grid>
        </Grid>
    </div>

    );
  } 
}

VueListeAnnonces.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withRouter(withStyles(styles)(VueListeAnnonces));
