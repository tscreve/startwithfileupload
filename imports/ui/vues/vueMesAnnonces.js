import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { withRouter, NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import { withTracker } from 'meteor/react-meteor-data'; 
import compose from 'recompose/compose';

import { Annonces } from '../../api/annonces.js';
import ListComp from '../comps/listComp.js';

import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    flexGrow: 1,
    // marginTop:40,
  },
  hideOnSmall:{
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
    // [theme.breakpoints.up('md')]: {
    //  display: 'block',
    // },
    // [theme.breakpoints.up('lg')]: {
    //  display: 'block',
    // },
  },
});

class myAnnonces extends Component {
  constructor(props) {
    super(props);

    this.state = {
      // ann:[],
    };    
  }

  componentDidMount(){
    // this.getFavorites();
  }

  componentDidUpdate(){
    // console.log(this.props.user[0].favorites.length)
    // console.log(this.state.favorites.length)
    // if(this.props.user[0].favorites && this.props.user[0].favorites.length !== this.state.favorites.length){
    //   this.getFavorites();
    // }

  }

  // getFavorites(){  
  //    Meteor.call('favorites.get',(err,res) => {
  //       if (err) {
  //         console.error('error', err)
  //       }else{
  //         this.setState({
  //           favorites:res,
  //         }); 
  //       }
  //    });
  // }
  render() {
    const { classes,ann } = this.props;
    const { favorites } = this.state;
    // console.log('render',type);

    return (
    <div>
      <Grid container  className={classes.root}>
        <Grid item xs={1} sm={1} md={2} className={classes.hideOnSmall}>
         
        </Grid>

        
          <Grid item xs={12} sm={12} md={8}>  
            <ListComp context="myAnnonces" items={ann} />
          </Grid>


        <Grid item xs={1} sm={1} md={2} className={classes.hideOnSmall}>
         
        </Grid>
      </Grid>
    </div>

    );
  } 
}

myAnnonces.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default compose(
  withStyles(styles),
  withTracker(() => {

  Meteor.subscribe('annonces.myAnnonces');
  return {
      ann: Annonces.find().fetch(),
  }
}),
)(myAnnonces);