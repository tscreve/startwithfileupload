import { Meteor } from 'meteor/meteor';
import { Tracker } from 'meteor/tracker';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { withRouter, NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import { withTracker } from 'meteor/react-meteor-data'; 
import compose from 'recompose/compose';
import { Session } from 'meteor/session';

import { Discussions } from '../../api/discussions.js';

import List from '../comps/virtualizedList.js';

import withWidth,{ isWidthUp } from '@material-ui/core/withWidth';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import CoreTextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import CircularProgress from '@material-ui/core/CircularProgress';

import Arrow from '@material-ui/icons/ArrowForward';

const styles = theme => ({
  root: {
    flexGrow: 1,
    // marginTop:40,
  },
  hideOnSmall:{
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
    // [theme.breakpoints.up('md')]: {
    //  display: 'block',
    // },
    // [theme.breakpoints.up('lg')]: {
    //  display: 'block',
    // },
  },
  text:{
    width:'62%',
    margin:'0 0 6px 15px',
  },
  div:{
    backgroundColor:'white',
    width:'100%',
    position:'fixed',
    bottom:0,
  },
  progressContainer:{
    backgroundColor:'white',
    textAlign:'center',
  },
  progress:{
    marginTop:200,
  },
});

class vueMessages extends Component {
  constructor(props) {
    super(props);
    const discussionId = this.props.location.state.discussionId ? this.props.location.state.discussionId : null;
    const objectId = this.props.location.state.objectId ? this.props.location.state.objectId : null;
 
    this.state = {
      discussionId:discussionId,
      objectId:objectId,
      text:"",
    }; 
    Session.set('subscribe',discussionId)
  }

  componentDidMount(){
    const{discussionId,objectId}=this.state;

    let id=""
    if(!discussionId){
       Meteor.call('discussions.getFromObject', objectId,(err, res) => {
        if(err){
          console.log(err)
        }else if(res){
          console.log(res) 
          Session.set('subscribe',res)
          this.setState({
            discussionId:res,
          });
        }
      }); 
    }
  }

  componentWillUnmount() {
    Session.set('subscribe',null)
  }

  insertMessages(){
     const {text,discussionId,objectId}=this.state;

     if(discussionId){
       Meteor.call('discussions.update', text,discussionId,(err, res) => {
        if(err){
          console.log(err)
          let error={};
          error.type=err.errorType
          error.message=err.message

          this.setState({
            errorInsert:true,
            error:error,
          });
        }else if(res){
          console.log(res)
        }
      }); 
     }else{
       Meteor.call('discussions.insert', text,objectId,(err, res) => {
        if(err){
          console.log(err)
          let error={};
          error.type=err.errorType
          error.message=err.message

          this.setState({
            errorInsert:true,
            error:error,
          });
        }else if(res){
          console.log(res)   
          Session.set('subscribe',res)
          this.setState({
            discussionId:res,
          });   
        }
      });
     }
  }

  handleChange(e){
    e.preventDefault();
    // console.log(e.target.value)
    this.setState({text:e.target.value});
  }

  submit(e){
    e.preventDefault();
    this.insertMessages()
  }

  render() {
    const { classes,width,discussion,ready } = this.props;
    const { discussionId,sub } = this.state;
    const msg = ready && discussion.length>0 ? discussion[0].messages : []

    return (
    <div>
      <Grid container  className={classes.root}>
        <Grid item xs={1} sm={1} md={2} className={classes.hideOnSmall}>
         
        </Grid>

        
          <Grid item xs={12} sm={12} md={8}>  
            <div>
            {
             msg && msg.length>0 || discussionId===null ?
               <List items={msg}/>     
              : 
              <div className={classes.progressContainer}>
                <CircularProgress className={classes.progress} />              
              </div>
            }
             

              <div className={classes.div}>
                <CoreTextField    
                    variant="outlined"
                    margin="dense"               
                    placeholder="Ecrire ici..."
                    // onKeyUp={(e) => this.submit(e)}
                    onChange={(e)=>this.handleChange(e)}
                    className={classes.text}
                    style={{width:!isWidthUp('md',width) && innerWidth-70}}              
                    />
                  <IconButton onClick={(e) => this.submit(e)}>
                     <Arrow color="primary" style={{transform:'rotate(270deg)'}}/>
                  </IconButton>
                </div>  
              </div>   
          </Grid>


        <Grid item xs={1} sm={1} md={2} className={classes.hideOnSmall}>
         
        </Grid>


      </Grid>
    </div>

    );
  } 
}

vueMessages.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default compose(
  withWidth(),
  withStyles(styles),
  withTracker(() => {
  const id=Session.get('subscribe');
  Meteor.subscribe('discussions.one',id)
  return {
    ready:Meteor.subscribe('discussions.one',id).ready(),
    discussion: Discussions.find({_id:id}).fetch()
  }
}),
)(withRouter(vueMessages));