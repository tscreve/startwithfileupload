
meteor npm install --save react react-dom
meteor add react-meteor-data
meteor add accounts-ui accounts-password alanning:roles
meteor remove insecure
meteor remove autopublish
meteor npm install --save classnames
meteor npm install --save react-router history react-router-dom

meteor npm install --save @material-ui/core @material-ui/icons
<!-- meteor npm install --save react-parallax -->

meteor add mrt:filesize ostrio:files
meteor npm install --save aws-sdk gm

meteor npm install --save leaflet react-leaflet opencage-api-client

meteor add aldeed:collection2@3.0.0
meteor npm install --save simpl-schema uniforms uniforms-material

meteor npm install --save react-id-swiper
meteor npm install --save react-swipeable-views
meteor npm install --save react-virtualized 
meteor npm install --save downshift
meteor npm install --save immutable

<!-- meteor npm install mobx-react --save -->

meteor npm install --save request